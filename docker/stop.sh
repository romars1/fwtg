#!/bin/bash
# FWTG
# Copyright (C) 2022  RomARS
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# 
# The code was developed by Mattia Quadrini <quadrini@romars.tech>
#

#if [ "$EUID" -ne 0 ]
#  then echo "Please run as root"
#  exit 1
#fi

for d in `docker ps --all | grep fwtg | awk '{print $1}'`
do  
  docker stop $d
  docker rm $d
done

for p in `ps aux | grep run.sh | awk '{print $2}'`
do 
  kill -9 $p
done