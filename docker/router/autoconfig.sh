#!/bin/bash

# FWTG
# Copyright (C) 2022  RomARS
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# 
# The code was developed by Mattia Quadrini <quadrini@romars.tech>
#

# DEBUG
#	devices=($(ip r | grep dev | grep -v default | awk '{printf "%s ",$3}'))
#
#	# DELAY/RATE config
#	for dev in $devices
#	do
#		tc qdisc add dev $dev root netem delay $delay rate $rate
#	done
#
#	# IP FORWARDING on
#	sysctl -w net.ipv4.conf.all.forwarding=1
#
#	# Replace ip address
#
#	for dev in $devices
#	do
#		ip address flush dev $dev
#	done

# Traffic shaping
# Depending on the order the LAN will be attached eth0 and eth1 are DL and UL
tc qdisc add dev eth0 root netem delay ${TC_DELAY}ms rate ${TC_RATE_DL}kbit limit ${BUF_PKTS_DL}
tc qdisc add dev eth1 root netem delay ${TC_DELAY}ms rate ${TC_RATE_UL}kbit limit ${BUF_PKTS_UL}

# Forwarding
sysctl -w net.ipv4.conf.eth0.forwarding=1 &> /dev/null
sysctl -w net.ipv4.conf.eth1.forwarding=1 &> /dev/null
sysctl -w net.ipv4.conf.all.forwarding=1 &> /dev/null

# Routing and Networking
#ip address flush dev eth0
#ip address add ${LAN_C}/24 dev eth0
#ip address flush dev eth1
#ip address add ${LAN_S}/24 dev eth1
#ip route delete default
#ip route delete default

bmon -p 'eth1' -r 1 -o format:fmt='$(element:name):\t TX $(attr:txrate:bytes) B\t RX $(attr:rxrate:bytes) B\n' > /var/log/fwtg-$(date +%d%m%y_%H%M).rate &

eth1_RX_name=$(date +%d%m%y_%H%M)
echo "`ip -s a s eth1 | grep -e RX`" > /var/log/eth1-RX-${eth1_RX_name}.log
eth1_TX_name=$(date +%d%m%y_%H%M)
echo "`ip -s a s eth1 | grep -e TX`" > /var/log/eth1-TX-${eth1_TX_name}.log
eth0_RX_name=$(date +%d%m%y_%H%M)
echo "`ip -s a s eth0 | grep -e RX`" > /var/log/eth0-RX-${eth0_RX_name}.log
eth0_TX_name=$(date +%d%m%y_%H%M)
echo "`ip -s a s eth0 | grep -e TX`" > /var/log/eth0-TX-${eth0_TX_name}.log


while [ true ] 
do 
    echo -e "`ip -s a s eth1 | grep -e RX -A2 | grep -v -e 'bytes  packets  errors  dropped'`" >> /var/log/eth1-RX-${eth1_RX_name}.log
    echo -e "`ip -s a s eth1 | grep -e TX -A2 | grep -v -e 'bytes  packets  errors  dropped'`" >> /var/log/eth1-TX-${eth1_TX_name}.log
    echo -e "`ip -s a s eth0 | grep -e RX -A2 | grep -v -e 'bytes  packets  errors  dropped'`" >> /var/log/eth0-RX-${eth0_RX_name}.log
    echo -e "`ip -s a s eth0 | grep -e TX -A2 | grep -v -e 'bytes  packets  errors  dropped'`" >> /var/log/eth0-TX-${eth0_TX_name}.log
    sleep 1
done 

#sleep 5
#sleep `bc <<< "${EMUL_TIME}+(${EMUL_TIME}*0.85)"`

sleep inf