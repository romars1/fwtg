# FWTG Docker deployment
The FWTG can be also used as Docker containers. A client and a server are prepared and launched in two different LANs. A router is implemented to setup the link with _rate_ and _delay_ characteristics. 


# Topology and Docker details
Run `./build.sh` in the `./docker` folder to build containers together or singularly  
 - FWTG-Seed  
   - FWTG-Server (namely server - based on FWTG-Seed)  
   - FWTG-Client (namely client - based on FWTG-Seed)  
 - FWTG-Router (namely router)  

A total of 4 docker images are stored locally, based on Ubuntu 20.04 focal-fossa. You can also customize packages into the client and server by modifying the FWTG-Seed or the single FWTG-<component>.  

The `../client` folder is copied in the `/root` folder of the client, similarly for the server (`../server`) and router (`./router`). 
Note that currently all components requires at least `NET_ADMIN` docker capabilities (`SYS_ADMIN` and `privileged` are also required for the router). See the `docker-compose.yaml`,`autoconfig.sh` files for more details. 

Two networks are created: 
 1. LAN_CLIENT (10.1.0.0/24)
 2. LAN_SERVER (10.2.0.0/24)

The router is attached to both networks with the statically IPv4 addresses `10.1.0.1` and `10.2.0.1`. It forwards packets on its interfaces setup with a certain delay and rate. 

The client is attached to the LAN_CLIENT with an IPv4 address assigend by the Docker daemon.

The server is attached to the LAN_SERVER with a statically IPv4 address `10.2.0.100` because it must be known by the client a-priory. 


## RUN with docker-compose
Use `docker-compose -f <compose-file> up` command to launch the emulation. You can modify the paramenters by editing the `docker-compose.yaml` file. In particular check the `environment` variables and networking.  

Use `run.sh` to schedule the start/stop of a test using docker-compose.

# Automator usage
It is recommended to use the `automator.sh` script to configure and launch a test using Docker environment. Multiple test can be launched by configuring variables in the script (make a copy), in particular network configuration:

```
# Variables
# --------------------------------------------------------------------
# ROUTER_LAN_C="10.1.0.1"
# ROUTER_LAN_S="10.2.0.1"
# ROUTER_TC_DELAY_ms="250" <=== provide as ms
# ROUTER_TC_RATE_DL_kbit="19950" <=== provide as kbit/s
# ROUTER_TC_RATE_UL_kbit="525" <=== provide as kbit/s
# ROUTER_BUF_PKTS_DL="3000"
# ROUTER_BUF_PKTS_UL="3000"
# SERVER_LAN_S="10.2.0.100"
# N_USERS="100"
# EMUL_TIME="1800"
# LAN_C="10.1.0" <== INDICATE THE LAST 3 BYTES WITHOUT THE DOT "."
# LAN_S="10.2.0" <== INDICATE THE LAST 3 BYTES WITHOUT THE DOT "."
# ROUNDS="100" <== HOW MANY TIME TO REPEAT THE TEST WITH THIS PARAMETERS
# P_NAME="user-100" <== PROJECT_FOLDER_NAME
# --------------------------------------------------------------------
```

