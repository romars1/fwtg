#!/bin/bash
# FWTG
# Copyright (C) 2022  RomARS
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# 
# The code was developed by Mattia Quadrini <quadrini@romars.tech>
#

#if [ "$EUID" -ne 0 ]
#  then echo "Please run as root"
#  exit 0
#fi

cd ../

case $1 in
  all | ALL | All)
  {
    docker image rm fwtg/client:dev
    docker image rm fwtg/server:dev
    docker image rm fwtg/seed:latest
    docker image rm fwtg/router:latest
    docker build . -t 'fwtg/seed:latest' --file ./docker/Dockerfile && 
    docker build . -t 'fwtg/client:dev' --file ./docker/Dockerfile.client && 
    docker build . -t 'fwtg/server:dev' --file ./docker/Dockerfile.server &&
    docker build . -t 'fwtg/router:dev' --file ./docker/Dockerfile.router && 
    echo -e "[OK] Build done withouth errors\n"
    docker image ls | grep fwtg
  } ||
  echo "[X] Something was wrong!"
  ;;

  seed | Seed | SEED)
  {
    docker image rm fwtg/seed:latest
    docker build . -t 'fwtg/seed:latest' --file ./docker/Dockerfile &&
    echo -e "[OK] Build done withouth errors\n"
    docker image ls | grep fwtg
  } ||
  echo "[X] Something was wrong!"
  ;;  

  client | Client | CLIENT)
  {
    docker image rm fwtg/client:dev
    docker build . -t 'fwtg/client:dev' --file ./docker/Dockerfile.client &&
    echo -e "[OK] Build done withouth errors\n"
    docker image ls | grep fwtg
  } ||
  echo "[X] Something was wrong!"
  ;;  
   
  server | Server | SERVER)
  {
    docker image rm fwtg/server:dev
    docker build . -t 'fwtg/server:dev' --file ./docker/Dockerfile.server &&
    echo -e "[OK] Build done withouth errors\n"
    docker image ls | grep fwtg
  } ||
  echo "[X] Something was wrong!"
  ;;  

  router | Router | ROUTER)
  {
    docker image rm fwtg/router:dev
    docker build . -t 'fwtg/router:dev' --file ./docker/Dockerfile.router &&
    echo -e "[OK] Build done withouth errors\n"
    docker image ls | grep fwtg
  } ||
  echo "[X] Something was wrong!"
  ;;  


  clean | Clean | CLEAN)
  {
    docker image remove fwtg/seed:latest fwtg/client:dev fwtg/server:dev fwtg/router:dev &&
    echo -e "[OK] Clean done withouth errors\n"
  } ||
  echo -e "[X] Something wrong\n"
  ;;

  * )
  clear
  echo "------------------------------------------------------------"
  echo "]  Please run as root"
  echo "]  Use build.sh without arguments to build all components"
  echo -e "]  Use build.sh with the following arguments:\
  \n \t all [seed + client + server];\
  \n \t seed; \
  \n \t client; \
  \n \t server; \
  \n \t router; \
  \n \t clean [remove all]"
  echo "------------------------------------------------------------"
  exit 0
  ;;

esac
