#!/bin/bash
# FWTG
# Copyright (C) 2022  RomARS
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# 
# The code was developed by Mattia Quadrini <quadrini@romars.tech>
#

#if [ "$EUID" -ne 0 ]
#  then echo "Please run as root"
#  exit 1
#fi

EMUL_TIME=$(grep -m 1 EMUL_TIME docker-compose.yaml | awk -F ":" '{print $2}' | awk -F "\"" '{print $2}')
NAME=test-$(date +%d%m%y_%H%M%S%n)
N_USERS=$(grep -m 1 N_USERS docker-compose.yaml | awk -F ":" '{print $2}' | awk -F "\"" '{print $2}')

start() {
  echo "Run.sh "
  date
  echo "--------------------------------"
  touch $NAME
  docker-compose -p $NAME up
}

stop() {
  docker-compose -p $NAME down
  echo "--------------------------------"
  echo "Ending "
  date
  echo "--------------------------------"
}

wait() {
  sleep 10; 
  sleep $N_USERS; 
  sleep $EMUL_TIME
}
case $1 in
  start)
    start
    ;;

  stop)
    stop
    ;;

  *)
    ( wait ; stop ) &
    start
    ;;
esac