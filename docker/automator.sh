#!/bin/bash
# FWTG
# Copyright (C) 2022  RomARS
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# 
# The code was developed by Mattia Quadrini <quadrini@romars.tech>
#


# Variables EXAMPLE
# --------------------------------------------------------------------
# ROUTER_LAN_C="10.1.0.1"
# ROUTER_LAN_S="10.2.0.1"
# ROUTER_TC_DELAY_ms="250" <=== provide as ms
# ROUTER_TC_RATE_DL_kbit="19950" <=== provide as kbit/s
# ROUTER_TC_RATE_UL_kbit="525" <=== provide as kbit/s
# ROUTER_BUF_PKTS_DL="3000"
# ROUTER_BUF_PKTS_UL="3000"
# SERVER_LAN_S="10.2.0.100"
# N_USERS="100"
# EMUL_TIME="1800"
# LAN_C="10.1.0" <== INDICATE THE LAST 3 BYTES WITHOUT THE DOT "."
# LAN_S="10.2.0" <== INDICATE THE LAST 3 BYTES WITHOUT THE DOT "."
# ROUNDS="100" <== HOW MANY TIME TO REPEAT THE TEST WITH THIS PARAMETERS
# P_NAME="user-100" <== PROJECT_FOLDER_NAME
# --------------------------------------------------------------------

# Variables
ROUTER_LAN_C="10.1.0.1"
ROUTER_LAN_S="10.2.0.1"
ROUTER_TC_DELAY_ms="250"
ROUTER_TC_RATE_DL_kbit="19635"
ROUTER_TC_RATE_UL_kbit="514"
ROUTER_BUF_PKTS_DL="300"
ROUTER_BUF_PKTS_UL="300"
SERVER_LAN_S="10.2.0.100"
NUM_USERS="50"
EMU_TIME="1800"
NET_LAN_C="10.1.0"
NET_LAN_S="10.2.0"
SERV_PORT="9000"
SERV_PORT1=$(expr $SERV_PORT + 1)
SERV_PORT2=$(expr $SERV_PORT + 2)
SERV_PORT3=$(expr $SERV_PORT + 3)
ROUNDS="4"
P_NAME="user50-plus5-(1)-buffer300"

echo "------------------------------ "
echo "FWTG Automator "
echo "------------------------------ "
echo "ROUTER_LAN_C = ${ROUTER_LAN_C}"
echo "ROUTER_LAN_S = ${ROUTER_LAN_S}"
echo "TC_RATE_DL = ${ROUTER_TC_RATE_DL_kbit} kbit/s"
echo "TC_RATE_UL = ${ROUTER_TC_RATE_UL_kbit} kbit/s"
echo "ROUTER_BUF_DL = ${ROUTER_BUF_PKTS_DL} packets"
echo "ROUTER_BUF_UL = ${ROUTER_BUF_PKTS_UL} packets"
echo "SERVER_LAN_S = ${SERVER_LAN_S}"
echo "NUM_USERS = ${NUM_USERS}"
echo "EMU_TIME = ${EMU_TIME} s"
echo "NET_LAN_C = ${NET_LAN_C}.0/24"
echo "NET_LAN_S = ${NET_LAN_S}.0/24"
echo "SERV_PORT = ${SERV_PORT}"
echo "ROUNDS = ${ROUNDS}"
echo "PROJECT_NAME = ${P_NAME}"
echo " ------------------------------ "

# Create the WORKDIR
WORKDIR="test-${P_NAME}-$(date +%d%m%y_%H%M%S)"
mkdir -p ./${WORKDIR}/
cp run.sh ./${WORKDIR}/
cp -r router ./${WORKDIR}/
cp -r client ./${WORKDIR}/
cp -r server ./${WORKDIR}/


# Create docker-compose file 
sed -e "s/ROUTER_LAN_C/${ROUTER_LAN_C}/g" \
-e "s/ROUTER_LAN_S/${ROUTER_LAN_S}/g" \
-e "s/ROUTER_TC_DELAY_ms/${ROUTER_TC_DELAY_ms}/g" \
-e "s/ROUTER_TC_RATE_DL_kbit/${ROUTER_TC_RATE_DL_kbit}/g" \
-e "s/ROUTER_TC_RATE_UL_kbit/${ROUTER_TC_RATE_UL_kbit}/g" \
-e "s/ROUTER_BUF_PKTS_DL/${ROUTER_BUF_PKTS_DL}/g" \
-e "s/ROUTER_BUF_PKTS_UL/${ROUTER_BUF_PKTS_UL}/g" \
-e "s/SERVER_LAN_S/${SERVER_LAN_S}/g" \
-e "s/NUM_USERS/${NUM_USERS}/g" \
-e "s/EMU_TIME/${EMU_TIME}/g" \
-e "s/NET_LAN_C/${NET_LAN_C}/g" \
-e "s/NET_LAN_S/${NET_LAN_S}/g" \
-e "s/SERV_PORT1/${SERV_PORT1}/g" \
-e "s/SERV_PORT2/${SERV_PORT2}/g" \
-e "s/SERV_PORT3/${SERV_PORT3}/g" \
-e "s/SERV_PORT/${SERV_PORT}/g" docker-compose-template.yaml > ./${WORKDIR}/docker-compose.yaml

# Move in the WORKDIR
cd ./${WORKDIR}

for i in `seq 1 ${ROUNDS}`
do 
    ./run.sh && 
    ( for f in `ls ./client/log/*.txt`; do cat $f | grep GET | grep custom | grep size | grep -v "size 0"; done | awk '{print $12}' | awk '{sum+=sprintf($1)}END{print sum}' > timingGETsum-${i}.dat ) &&
    ( for f in `ls ./client/log/*.txt`; do cat $f | grep GET | grep custom | grep size | grep -v "size 0"; done | awk '{print $12}' | cat -n | tail -n1 | awk '{print $1}' > timingGETnumberOfelements-${i}.dat) &&
    ( for f in `ls ./client/log/*.txt`; do cat $f | grep POST | grep custom | grep size | grep -v "size 0"; done | awk '{print $12}' | awk '{sum+=sprintf($1)}END{print sum}' > timingPOSTsum-${i}.dat ) &&
    ( for f in `ls ./client/log/*.txt`; do cat $f | grep POST | grep custom | grep size | grep -v "size 0"; done | awk '{print $12}' | cat -n | tail -n1 | awk '{print $1}' > timingPOSTnumberOfelements-${i}.dat) &&
    tGetSum=$(cat timingGETsum-${i}.dat);
    tGetNum=$(cat timingGETnumberOfelements-${i}.dat);
    bc -l <<< "$tGetSum/$tGetNum" > timingGET-${i}.txt;
    tPostSum=$(cat timingPOSTsum-${i}.dat);
    tPostNum=$(cat timingPOSTnumberOfelements-${i}.dat);
    bc -l <<< "$tPostSum/$tPostNum" > timingPOST-${i}.txt;
    ( 
        echo "To file... "
        zip ./test-${P_NAME}-${i}.zip ./client/log/log* ./router/* &> /dev/null ;  
        rm ./client/log/log* &> /dev/null ;
        rm ./router/rate/* &> /dev/null ;
        echo "--------------------------------";
        sleep 5;
    )
done