# Flexible Web Traffic Generator
Please, use the following sentence for references and citations
```
M. Luglio, M. Quadrini, C. Roseti, F. Zampognaro,
A Flexible Web Traffic Generator for the dimensioning of a 5G backhaul in NPN,
Computer Networks,
2022,
109531,
ISSN 1389-1286,
https://doi.org/10.1016/j.comnet.2022.109531.
```


## Objective
This tool is aimed for the emulation of web traffic using HTTP GET and POST method requests. 

## Tool Structure
The tool provides a client-side (client) and a server-side (server)software that must be run on separated or same machine (i.e., VM or physical server). The server must be reachable by the client.

## Definitions
### User
It is defined as the physical person who start the program.

### Web user
It is defined as a virtual entity that performs HTTP requests of objects with a given size and rate. It is characterized by a set of weights associated to each web traffic category and a user activity time that represents the maximum time the web user is in an IDLE state after the completion of a Session. 

### Emulation
It is defined as the main process started by the user and terminated after a given time. It is characterized by a duration expressed in seconds and will consists of a single web usdr and one or multiple Sessions. 

### Session
It is defined as the continuous activity of a web user inside the emulation process. It is characterized by a duration expressed in seconds and a web traffic category. It consists of one or multiple interaction modes. Two Session are separated in time by a random IDLE time set according to the user profile. 

### Interaction Mode
It is defined as a set of GET or POST requests performed considering a target request rate and object size distributions. It is characterized by a duration expressed in number of object to request. 

## Deployment Modes
### Docker
Check the documentation in the `docker` folder. Dockerfiles were prepared to create three containers:
 - `fwtg/client` : it represents one or more web clients running the FWTG software (client side)  
 - `fwtg/server` : it represents one or more web server running the FWTG software (server side)
 - `fwtg/router` : it models the link between client and server by setting the link bottleneck capacity, delay, buffer (...). 

## Extension
Some extensions are provided in the `client/extension` folder to emulate non-HTTP traffic such as Sensor, VoIP, live streaming (video call) for different purposes. As well, it is possible to generate live random `user_profile.json` file for Montecarlo simulations. 
