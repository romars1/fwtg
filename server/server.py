# FWTG
# Copyright (C) 2022  RomARS
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# 
# The code was developed by Mattia Quadrini <quadrini@romars.tech>
#

import socket
from http.server import BaseHTTPRequestHandler, HTTPServer, ThreadingHTTPServer
import time
import urllib.parse as urlparse
import random
import string
import iperf3
import threading
import sys

print("==============")
print("| FWTG-Server |")
print("==============")

# v1.2
# - Updated with ThreadingHTTPServer; Python >3.7 is required!

# v1.3
# - Support to HTTP1/1 and connection keep-alive!
# - Fixed do_GET and do_POST

if (len(sys.argv)<1):
	print("] Missing Server Port (integer number)")
else:
	hostPort = 8088

hostName = "0.0.0.0"
#hostPort = 8088
hostPort=int(sys.argv[1])
#iperfPort = hostPort+1

class MyServer(BaseHTTPRequestHandler):
	protocol_version='HTTP/1.1'
	def do_GET(self):
		size = urlparse.parse_qs(urlparse.urlparse(self.path).query).get('size', 0)[0]
		onek=''.join(random.choice(string.ascii_lowercase+string.punctuation+string.digits) for x in range(1000))
		msg=''.join(onek for x in range(int(size)))
		body=msg.encode("utf-8")
		self.send_response(200)
		self.send_header("Content-Length", len(msg))
		self.send_header('Content-Type','text/html')
		self.end_headers()
		self.wfile.write(body)
		#self.wfile.write(bytes("<html><body><p>Requested size: %i KB</p> %s </body></html>" % (int(size),msg), "utf-8"))

	def do_POST(self):
		content_length = int(self.headers['Content-Length']) 
		content=self.rfile.read(content_length).decode('utf-8')
		response=content.encode('utf-8')
		self.send_response(200)
		self.send_header('Content-Length', len(content))
		self.send_header('Content-Type', 'text/plain')
		self.end_headers()
		self.wfile.write(response)
		#self.wfile.write("<html><body><p>POST OK! Size: %i bytes</body></html>"%content_length)

myServer = ThreadingHTTPServer((hostName, hostPort), MyServer)
print(time.asctime(), "Server Starts - %s:%s" % (hostName, hostPort))

def iperf_Server(ip,port):
	try:
		while(True):
			s=iperf3.Server()
			s.bind_address=ip
			s.port=port
			s.verbose=True 
			s.run()
	except Exception as e:
		print("Error in iperf_Server:",e)
		sys.exit()
	except KeyboardInterrupt:
		pass

try:
	#th=threading.Thread(target=iperf_Server, args=(hostName,iperfPort))
	#th.start()
	myServer.serve_forever()
except Exception as e:
	print("Error!",e)
except KeyboardInterrupt:
	pass

myServer.server_close()
print(time.asctime(), "Server Stops - %s:%s" % (hostName, hostPort))