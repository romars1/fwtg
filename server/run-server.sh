#!/bin/bash
# FWTG
# Copyright (C) 2022  RomARS
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# 
# The code was developed by Mattia Quadrini <quadrini@romars.tech>
#

if [ "$#" -eq 0 ] || [ "$#" -gt 4 ] || [ "$#" -gt 1 ] && [ "$#" -lt 4 ]
then
	echo "============FWTG-SERVER============"
	echo "]"
	echo "] Inputs:"
	echo "] - HTTP Server Port"
	echo "] - Iperf3 Port Service1"
	echo "] - Iperf3 Port Service2"
	echo "] - Iperf3 Port Service3"
	echo "]"
	echo "] Services are run over 0.0.0.0"
	echo "] Use 'clear' to kill processes"
	echo "]"
	echo "=================================="
	exit 0
fi

if [ "$#" -eq 1 ] && [ "$1" == "clear" ]
then
	echo "] Iperf3 services:"
	ps aux | grep iperf3
	echo "] Killing services..."
	killall iperf3
	echo "] FWTG-Server service:"
	ps aux | grep "python3.9 server.py"
	echo "] Killing service..."
	killall python3.9
	exit 0
fi

http_port=$1
iperf_1=$2
iperf_2=$3
iperf_3=$4

iperf3 -s -D -p $iperf_1
echo ""
iperf3 -s -D -p $iperf_2
echo ""
iperf3 -s -D -p $iperf_3
echo ""
python3.9 server.py $http_port &> /var/log/server-1.log &