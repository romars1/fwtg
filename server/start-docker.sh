#!/bin/bash
# FWTG
# Copyright (C) 2022  RomARS
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# 
# The code was developed by Mattia Quadrini <quadrini@romars.tech>
#

./autoconfig.sh
if [ ${LOG} ]
then 
    python3.9 server.py $SERVER_PORT &> /var/log/server-1.log &
    python3.9 server.py $(bc <<< "$SERVER_PORT+1")  &> /var/log/server-2.log &
    python3.9 server.py $(bc <<< "$SERVER_PORT+2")  &> /var/log/server-3.log &
    python3.9 server.py $(bc <<< "$SERVER_PORT+3")  &> /var/log/server-4.log &
else
    python3.9 server.py $SERVER_PORT &> /dev/null &
    python3.9 server.py $(bc <<< "$SERVER_PORT+1")  &> /dev/null &
    python3.9 server.py $(bc <<< "$SERVER_PORT+2")  &> /dev/null &
    python3.9 server.py $(bc <<< "$SERVER_PORT+3")  &> /dev/null &
fi

#sleep `bc <<< "${EMUL_TIME}+(${EMUL_TIME}*0.85)"`
sleep inf