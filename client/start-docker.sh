#!/bin/bash
# FWTG
# Copyright (C) 2022  RomARS
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# 
# The code was developed by Mattia Quadrini <quadrini@romars.tech>
#

./autoconfig.sh

# Generate random profile and activity
python3.9 ./extension/random-profile.py ${N_USERS}
python3.9 ./extension/random-activity.py ${N_USERS}

# Creat backup user_profile.json
if ! [ -e  ./json/user_profile.json.bak ]
then
    cp ./json/user_profile.json ./json/user_profile.json.bak
fi

for i in `seq 1 ${N_USERS}`
do
    if [ "$i" -gt 75 ]
    then
        PORT=$(bc <<< "$SERVER_PORT+3")
    elif [ "$i" -gt 50 ]
    then
        PORT=$(bc <<< "$SERVER_PORT+2")
    elif [ "$i" -gt 25 ]
    then
        PORT=$(bc <<< "$SERVER_PORT+1")
    else
        PORT=$SERVER_PORT
    fi
    # For each user extract the values
    a=$(sed -n ${i}p ./random_activity.csv)
    n=$(sed -n ${i}p ./random_profile.csv | awk -F '[\\[\\]]' '{print $2}' | awk -F ',' '{print $1}')
    sn=$(sed -n ${i}p ./random_profile.csv | awk -F '[\\[\\]]' '{print $2}' | awk -F ',' '{print $2}')
    ms=$(sed -n ${i}p ./random_profile.csv | awk -F '[\\[\\]]' '{print $2}' | awk -F ',' '{print $3}')
    c=$(sed -n ${i}p ./random_profile.csv | awk -F '[\\[\\]]' '{print $2}' | awk -F ',' '{print $4}')
    # Replace the values in the user_profile.json file
    jq ".custom.user_activity=${a} | .custom.newspaper=${n} | .custom.social_network=${sn} | .custom.media_streaming=${ms} | .custom.collaborative=${c}" ./json/user_profile.json.bak > ./json/user_profile.json
    
    # Execute the fwtg with the custom file
    if ! [ -d "log" ]
    then
        mkdir log
    fi
    userIndex=1
    name="log-user-"${userIndex}".txt"
    while [ true ]
    do
        if [ -e "./log/${name}" ]; 
        then 
            userIndex=`expr ${userIndex} + 1`
            name="log-user-"${userIndex}".txt"
        else 
            name="log-user-"${userIndex}".txt"
            break
        fi
    done

    python3.9 ./main.py custom ${EMUL_TIME} ${SERVER_IP} ${SERVER_PORT} verbose &> ./log/${name}

    sleep 1
done

rm ./random_activity.csv && rm ./random_profile.csv

#sleep `bc <<< "${EMUL_TIME}+(${EMUL_TIME}*0.85)"`
sleep inf