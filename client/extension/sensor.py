# FWTG
# Copyright (C) 2022  RomARS
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# 
# The code was developed by Mattia Quadrini <quadrini@romars.tech>
#

from module.config import User,logPath,jsonPath,jsonUser
from module.client import *
import sys
import random
import time
import json
import os


def sensor_Parser(pathJSON,fileJSON,_Profile):
	try:
		# From the user_profile reads information about sensot, i.e.:
		#  - message_per_hour
		#  - message_size (in kB)
		#  - transmission_mode [uplink / downlink / bidirectional]
		f1=open(pathJSON+fileJSON)
		userJson=json.load(f1)
		frequency=int(userJson[_Profile]["message_per_h"])
		size=int(userJson[_Profile]["message_size"])
		mode=str(userJson[_Profile]["transmission_mode"])
		return frequency,size,mode
	except Exception as e:
		print("Error in sensor_Parser: ",e)
		print('Frequency:',frequency,'Size:',size)
		sys.exit()

def run_Sensor(_UserProfile,_EmulationTime,_DstIP,_DstPort,_Verbose):
	try:
		# Create log file in ./log
		userIndex=1
		name='log-user-'+str(_UserProfile)+'-'+str(userIndex)+'.txt'
		while (name in os.listdir(os.path.abspath(logPath))):
			userIndex=userIndex+1
			name='log-user-'+str(_UserProfile)+'-'+str(userIndex)+'.txt'
		log=open(logPath+name,'w')
		# mph MessagePerHour ; mSize MessageSize (kB)
		mph,mSize,mode=sensor_Parser(jsonPath,jsonUser[0],_UserProfile)
		# Send 1 request every 'frequency' sencods: every 3600s mph requests are
		#  generated
		frequency=3600/mph
		sensorStart=time.time()
		print("–",round(time.time()-sensorStart),"Emulation Started")
		print("–",round(time.time()-sensorStart),"Emulation Started",file=log)
		while(True):
			if (time.time() >= sensorStart+_EmulationTime): break 
			session=requests.Session()
			if (mode=='uplink'):
				method='POST'
			elif (mode=='downlink'):
				method='GET'
			elif (mode=='bidirectional'):
				method=random.choices(['GET','POST'],weights=[50,50],k=1)[0]
			else:
				method=random.choices(['GET','POST'],weights=[50,50],k=1)[0]
			if (method=='GET'):
				status,delta,objSize=makeGET(session,_DstIP,_DstPort,mSize)
			elif (method=='POST'):
				status,delta,objSize=makePOST(session,_DstIP,_DstPort,mSize)
			else:
				raise Exception('No method provided')
			print("|",round(time.time()-sensorStart,6),_UserProfile,userIndex,method,status,'size',objSize,'timing',round(delta,3),file=log)
			if (_Verbose): print("|",round(time.time()-sensorStart,6),_UserProfile,userIndex,method,status,'size',objSize,'timing',round(delta,3))
			for t in range (1,int(frequency)+1):
				if (time.time() >= sensorStart+_EmulationTime): break 
				time.sleep(1)
		return 0
	except Exception as e:
		print("Error run_Sensor:",e)
