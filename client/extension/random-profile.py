# FWTG
# Copyright (C) 2022  RomARS
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# 
# The code was developed by Mattia Quadrini <quadrini@romars.tech>
#
# This python script shall be used to genereta random user_profile concerning 
# the FWTG to perform a Montecarlo simulation. The user_profile is composed by 
# 4 %-values: 
# [ Newspaper, Social Network, Media Streaming, Collaborative ]
#
# Indicate the number of clients (n_client); the output will be the 
# 'random_profile.csv' file containing 'n_client' different profiles. 
# Note that the output will be overwritten. Default value=10.

import random
import string
import sys

n_client=10
csv=open('./'+"random_profile.csv",'w')

try:
	if (len(sys.argv)>1):
		n_client=int(sys.argv[1])
#	else:
#		print("n_client",n_client)
except Exception as e:
	print("Error:",e)
	sys.exit()

#print("Provided",str(n_client),"n_client as input")
#print("Elaborating...")

for c in range(0,n_client):
	profile=[]
	while (sum(profile)<100):
		profile.append(random.randrange(0,100,5))
		if (sum(profile)>100) or (len(profile)>4) or ((sum(profile)==100) and (len(profile)<4)):
			profile=[]
			continue
	
#	print("client-"+str(c+1)+":",profile,"| SUM=",sum(profile))
	print(profile,file=csv)