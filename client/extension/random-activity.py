# FWTG
# Copyright (C) 2022  RomARS
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# 
# The code was developed by Mattia Quadrini <quadrini@romars.tech>
#


# -- DESCRITPION --
# This python script shall be used to genereta random user_activity concerning the FWTG to perform a Montecarlo simulation. 
#
# -- HOWTO --
# Indicate the number of clients (n_client); the output will be the 'random_activity.csv' file containing 'n_client' different activitys. Note that the output will be overwritten. Default value=10.

import random
import string
import sys

n_client=1
csv=open('./'+"random_activity.csv",'w')

try:
	if (len(sys.argv)>1):
		n_client=int(sys.argv[1])
#	else:
#		print("n_client",n_client)
except Exception as e:
	print("Error:",e)
	sys.exit()

#print("Provided",str(n_client),"n_client as input")
#print("Elaborating...")

for c in range(0,n_client):
	activity=random.randrange(5,100,5)
	#print("client-"+str(c+1)+":",activity)
	print(activity,file=csv)