#!/bin/bash

# FWTG
# Copyright (C) 2022  RomARS
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# 
# The code was developed by Mattia Quadrini <quadrini@romars.tech>
#

ip=$1
video_rate=3M #kbit/s
voice_rate=128K #kbit/s
time=900

# Video 3Mbit/s UL
iperf3 -c $ip -p 8090 -b $video_rate -t $time &

# Voice 128kbit/s UL/DL
iperf3 -c $ip -p 8091 -b $voice_rate -t $time &
iperf3 -c $ip -p 8092 -b $voice_rate -R -t $time &

