# FWTG
# Copyright (C) 2022  RomARS
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# 
# The code was developed by Mattia Quadrini <quadrini@romars.tech>
#

from module.config import User,logPath,jsonPath,jsonUser
from module.config import create_Session,create_Idle
from module.client import *
import sys
import random
import time
import json
import os
import iperf3


def emergency_Parser(pathJSON,fileJSON,_Profile):
	try:
		# From the user_profile reads information about sensot, i.e.:
		#  - time (duration expressed in seconds)
		#  - data_rate (kbit/s)
		f1=open(pathJSON+fileJSON)
		userJson=json.load(f1)
		duration=int(userJson[_Profile]["time"])
		rate=int(userJson[_Profile]["data_rate"])*1000
		activity=int(userJson[_Profile]["user_activity"])
		return duration,rate, activity
	except Exception as e:
		print("Error in emergency_Parser: ",e)
		print('Duration:',duration,'Rate:',rate)
		sys.exit()

def run_Emergency(_UserProfile,_EmulationTime,_DstIP,_DstPort,_Verbose):
	try:
		print(_UserProfile)
		if (_UserProfile=='medical_video'):
			dPort=int(_DstPort)+1
		elif (_UserProfile=='medical_voice'):
			dPort=int(_DstPort)+2
		else:
			dPort=5555
		# Create log file in ./log
		print(dPort)
		userIndex=1
		name='log-user-'+str(_UserProfile)+'-'+str(userIndex)+'.txt'
		while (name in os.listdir(os.path.abspath(logPath))):
			userIndex=userIndex+1
			name='log-user-'+str(_UserProfile)+'-'+str(userIndex)+'.txt'
		log=open(logPath+name,'w')
		# Parse values
		duration,rate,activity=emergency_Parser(jsonPath,jsonUser[0],_UserProfile)
		# Create Session duration vector
		activityTime=_EmulationTime*activity/100
		sessionTime=create_Session(_EmulationTime, activityTime)
		sessionTimeIndex=0
		# Evaluate total time of Idle
		totalIdleTime=_EmulationTime-activityTime
		# Create Idle vector
		idleTime=create_Idle(totalIdleTime)
		idleTimeIndex=0
		emulationVector=[]
		emulationIndex=0
		for v in range(0,len(sessionTime)):
			emulationVector.append("S")
		for v in range(0,len(idleTime)):
			emulationVector.append("I")
		random.shuffle(emulationVector)
		random.shuffle(emulationVector)
		print(emulationVector)
		# Start of the Emulation
		emergencyStart=time.time()
		print("–",round(time.time()-emergencyStart,3),"Emulation Started")
		print("–",round(time.time()-emergencyStart,3),"Emulation Started",file=log)
		while(True):
			if (time.time() >= emergencyStart+_EmulationTime): break 
			if (emulationVector[emulationIndex] == 'I'):
					idle=idleTime[idleTimeIndex]
					print("|",round(time.time()-emergencyStart,3),_UserProfile,userIndex,'IDLE ('+str(idle)+')',file=log)
					if (_Verbose): print("|",round(time.time()-emergencyStart,3),_UserProfile,userIndex,'IDLE ('+str(idle)+')')
					for t in range(1,idle+1):
						if (time.time() >= emergencyStart+_EmulationTime): break 
						time.sleep(1)
					idleTimeIndex=idleTimeIndex+1
					emulationIndex=emulationIndex+1
			elif (emulationVector[emulationIndex] == 'S'):
					sessionTimeIndex=sessionTimeIndex+1
					emulationIndex=emulationIndex+1
					print("|",round(time.time()-emergencyStart,3),_UserProfile,userIndex,'New Session',file=log)
					if (_Verbose): print("|",round(time.time()-emergencyStart,3),_UserProfile,userIndex,'New Session')
					try:
						transmission=0%2; # 0 = UL ; 1 = DL
						for mode in range(0,10):
							# Client Iperf3 setup
							c=iperf3.Client()
							print("Client created")
							c.duration=int(duration/10)
							print("Duration:",c.duration,"s")
							c.server_hostname=_DstIP
							print("ServerIP:",c.server_hostname)
							c.port=dPort
							print("ServerPort:",c.port)
							c.bandwidth=rate
							print("Data-Rate:",c.bandwidth)
							c.protocol='tcp'
							print("Client Protocol:",c.protocol)
							c.blksize=1000 # Set this for bulk transfer
							c.verbose=False # Set this for output
							if (transmission==1):
								c.reverse=True
							else:
								c.reverse=False
							if (_Verbose): print("|",round(time.time()-emergencyStart,3),_UserProfile,userIndex,'duration',c.duration,'s','bandwidth',c.bandwidth,'bit/s','protocol',c.protocol)
							print("|",round(time.time()-emergencyStart,3),_UserProfile,userIndex,'duration',c.duration,'s','bandwidth',c.bandwidth,'bit/s','protocol',c.protocol,file=log)
							c.run()
							#if (_Verbose):
							print("|",round(time.time()-emergencyStart,3),_UserProfile,userIndex,"Test completed:")
							#print(result)
							transmission=(transmission+1)%2
							# Reset to default
							c.defaults() # Important: reset iperf3 to default before start a new transmission
							time.sleep(0.5) # Important: wait some time
							print("-----------------")
					except KeyboardInterrupt:
						pass
					except Exception as e:
						print("Error during Session:",e)
						sys.exit()
		return 0
	except Exception as e:
		print("Error run_Emergency:",e)
