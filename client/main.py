# FWTG
# Copyright (C) 2022  RomARS
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# 
# The code was developed by Mattia Quadrini <quadrini@romars.tech>
#

from module.loader import *
from extension.sensor import *
from extension.emergency import *
import os
import sys

os.system('clear')
if (len(sys.argv)>1) and ((sys.argv[1]=='info') or (sys.argv[1]=='version')):
	print("] Available inputs:")
	print("]  User Profile (Social - Streaming - Business - Medical-[Data/Voice/Video] - IoT - Green)")
	print("]  Simultation time (integer number)")
	print("]  Destination IPv4 address (string, e.g. '192.168.0.3')")
	print("]  Destination port (integer number)")
	print("]  Options (verbose)")
	print('')
	sys.exit()

try:
	if (len(sys.argv)>1) and ((sys.argv[1]=='help') or (sys.argv[1]=='?')):
		print("] Available inputs:")
		print("]  User Profile (Social - Streaming - Business - Custom - Medical-[Data/Voice/Video] - IoT - Green)")
		print("]  Simultation time (integer number)")
		print("]  Destination IPv4 address (string, e.g. '192.168.0.3')")
		print("]  Destination port (integer number)")
		print("]  Options (verbose)")
		print('')
		sys.exit()

except Exception as e:
	print("Error",e)
	sys.exit()

# Input
userProfile='social'
emulationTime=900
dstIP='localhost'
dstPort=8088
verbose=False
try:
	if (len(sys.argv)>1):
		userProfile=str.lower(sys.argv[1])		# User profile
		if (userProfile=='medical-data'):
			userProfile=='medical_data'
		elif (userProfile=='medical-voice'):
			userProfile=='medical_voice'
		elif (userProfile=='medical-video'):
			userProfile=='medical_video'
		emulationTime=int(sys.argv[2])	# Duration of the emulation
		dstIP=str(sys.argv[3])	# Destination Server IPv4 address
		dstPort=int(sys.argv[4])	# Destination
		if (len(sys.argv)>5):
			if (sys.argv[5]=='verbose'):
				verbose=True			
	else:
		print("Default parameters set")
except IndexError:
	print('------------------')
	print('	ERROR			')
	print('------------------')
	print("] Missing inputs:")
	print("]  User Profile (Social - Streaming - Business - Custom - Medical-[Data/Voice/Video] - IoT - Green)")
	print("]  Simultation time (integer number)")
	print("]  Destination IPv4 address (string, e.g. '192.168.0.3')")
	print("]  Destination port (integer number)")
	print("]  Options (verbose)")
	sys.exit()
except ValueError:
	print('------------------')
	print('	ERROR			')
	print('------------------')
	print("]Please check input format")
	print("]  User Profile (Social - Streaming - Business - Custom - Medical-[Data/Voice/Video] - IoT - Green)")
	print("] Simultation time (integer number)")
	print("] Destination IPv4 address / hostname (string)")
	print("] Destination port (integer number)")
	print("] Options (verbose)")
	sys.exit()
except Exception as e:
	print('------------------')
	print('	ERROR			')
	print('------------------')
	print("Error",e)
	sys.exit()


def main(_userProfile,_emulationTime,_dstIP,_dstPort,_verbose):
	try:
		if (_userProfile=='sensor' or _userProfile=='green' or _userProfile=='medical_data'):
			run_Sensor(_userProfile,_emulationTime,_dstIP,_dstPort,_verbose)
		elif (_userProfile=='medical_voice' or _userProfile=='medical_video'):
			run_Emergency(_userProfile,_emulationTime,_dstIP,_dstPort,_verbose)
		else:
			run_Emulation(_userProfile,_emulationTime,_dstIP,_dstPort,_verbose)
	except Exception as e:
		print("Error in main",e)
		sys.exit()
	except KeyboardInterrupt:
		print("The emulation is interrupted")
		pass
		sys.exit()

if __name__=="__main__":
	print('')
	print("Emulation parameters:")
	print(" - User Profile",userProfile)
	print(' - Emulation time:',str(emulationTime)+'s')
	print(' - Destination IPv4:',dstIP)
	print(' - Destination port:',dstPort)
	print(" - Verbose",verbose)
	print('')
	main(userProfile,emulationTime,dstIP,dstPort,verbose)



