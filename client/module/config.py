# FWTG
# Copyright (C) 2022  RomARS
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# 
# The code was developed by Mattia Quadrini <quadrini@romars.tech>
#


import sys
import random
import math
import time
import json
import csv
import concurrent.futures
import string
import threading
import subprocess
import os
import signal


csvPath='./csv/'
jsonPath='./json/'
logPath='./log/'


jsonUser=['user_profile.json']
csvNewspaperNI=['reqrate-newspaper-NI-GET.csv','reqrate-newspaper-NI-POST.csv','size-newspaper-NI-GET.csv','size-newspaper-NI-POST.csv'] 

csvNewspaperI=['reqrate-newspaper-I-GET.csv','reqrate-newspaper-I-POST.csv','size-newspaper-I-GET.csv','size-newspaper-I-POST.csv']

csvNewspaperFI=['reqrate-newspaper-FI-GET.csv','reqrate-newspaper-FI-POST.csv','size-newspaper-FI-GET.csv','size-newspaper-FI-POST.csv']

csvSocialNI=['reqrate-social-NI-GET.csv','reqrate-social-NI-POST.csv','size-social-NI-GET.csv','size-social-NI-POST.csv']

csvSocialI=['reqrate-social-I-GET.csv','reqrate-social-I-POST.csv','size-social-I-GET.csv','size-social-I-POST.csv']

csvSocialFI=['reqrate-social-FI-GET.csv','reqrate-social-FI-POST.csv','size-social-FI-GET.csv','size-social-FI-POST.csv']

csvStreamingNI=['reqrate-streaming-NI-GET.csv','reqrate-streaming-NI-POST.csv','size-streaming-NI-GET.csv','size-streaming-NI-POST.csv']

csvStreamingP=['reqrate-streaming-P-GET.csv','reqrate-streaming-P-POST.csv','size-streaming-P-GET.csv','size-streaming-P-POST.csv']

csvCollaborativeNI=['reqrate-collaborative-NI-GET.csv','reqrate-collaborative-NI-POST.csv','size-collaborative-NI-GET.csv','size-collaborative-NI-POST.csv']

csvCollaborativeI=['reqrate-collaborative-I-GET.csv','reqrate-collaborative-I-POST.csv','size-collaborative-I-GET.csv','size-collaborative-I-POST.csv']

csvCollaborativeFI=['reqrate-collaborative-FI-GET.csv','reqrate-collaborative-FI-POST.csv','size-collaborative-FI-GET.csv','size-collaborative-FI-POST.csv',]

#single=['reqrate-kernel-NI-GET.csv', 'size-kernel-NI-GET.csv']


class User:
	# User object. Initialize the User with a profile. 
	# Later you can add the frequency activity and other related info
	def __init__(self, profile):
		self.profile=str(profile)
		
	def add_activity(self,_UserActivity):
		self.activity=int(_UserActivity)
		# Percentage of emualtionTime the user is active
		# For the sensor is the message_per_hour

	def add_service(self,*args):
		self.service=args[0]


class Session:
	# Session object. Initialize Session with full info
	def __init__(self, _ServiceCategory, _Duration):
		self.category=str(_ServiceCategory)
		self.duration=int(_Duration)


def create_Session(_emulationTime, _activityTime):
	try:
		duration=[]
		if (_activityTime<300):
			duration.append(_activityTime)
		else:
			while (sum(duration)<_activityTime):
				duration.append(random.randrange(150,600,1)) 
				if (sum(duration)>_activityTime):
					duration=[]
					continue
		return duration
	except Exception as e:
		print("Error in create_Session",e)
		sys.exit()

def create_Idle(_totalIdle):
	try:
		tIdle=[]
		if (_totalIdle<300):
			tIdle.append(_totalIdle)
		else:
			while (sum(tIdle)<_totalIdle):
				tIdle.append(random.randrange(150,600,1)) 
				if (sum(tIdle)>_totalIdle):
					tIdle=[]
					continue
		return tIdle
	except Exception as e:
		print("Error in create_Idle",e)
		sys.exit()

def random_Category(user, _ServiceCategory):
	try: 
		service=str(random.choices(_ServiceCategory,weights=user.service,k=1)[0]) 
		return service
	except Exception as e:
		print("Error in random_Session",e)
		sys.exit()

def user_Service(pathJSON,fileJSON):
	# ToDo: renderlo generico aggiungendo come input il profilo. 
	# 		Quindi ritornerà solo una probabilità del profilo richiesto
	# Returns an array containing the weights (%) of the kind of service 
	# each kind of user is allowed to do
	# E.g.: User Social: 0% Newspaper ; 50% Social-Network; 
	# 					50% Media-Streaming; 0% Collaborative
	# 		User Streaming: 0% Newspaper ; 30% Social-Network; 
	# 					70% Media-Streaming; 0% Collaborative
	# 		User Business: 50% Newspaper ; 10% Social-Network; 
	# 					10% Media-Streaming; 30% Collaborative
	try:
		f1=open(pathJSON+fileJSON)
		userJson=json.load(f1)
		probabilities=[[],[],[],[]]
		probabilities[0]=[userJson['social']['newspaper'],userJson['social']['social_network'],userJson['social']['media_streaming'],userJson['social']['collaborative']]
		probabilities[1]=[userJson['streaming']['newspaper'],userJson['streaming']['social_network'],userJson['streaming']['media_streaming'],userJson['streaming']['collaborative']]
		probabilities[2]=[userJson['business']['newspaper'],userJson['business']['social_network'], userJson['business']['media_streaming'],userJson['business']['collaborative']]
		probabilities[3]=[userJson['custom']['newspaper'],userJson['custom']['social_network'], userJson['custom']['media_streaming'],userJson['custom']['collaborative']]
		return probabilities
	except FileNotFoundError:
		print("Error in user_Service",e)
		sys.exit()


def user_Activity(pathJSON,fileJSON):
	try:
		# ToDo: IDEM per user_Service
		# Returns an array of user frequency activity: 
		# the time after which a new interaction-mode is selected 
		f1=open(pathJSON+fileJSON)
		userJson=json.load(f1)
		frequency=[[],[],[],[]]
		frequency[0]=int(userJson["social"]["user_activity"])
		frequency[1]=int(userJson["streaming"]["user_activity"])
		frequency[2]=int(userJson["business"]["user_activity"])
		frequency[3]=int(userJson["custom"]["user_activity"])
		return frequency
	except Exception as e:
		print("Error in user_Activity",e)
		sys.exit()


def probability_CSV(*args):
	try:
		# Takes as input any csv file(s) containing two columns: 
		# Value (ReqRate/Size/Other) | Probability
		# and returns an array of the type [ [ [NI-Rate],[NI-Prob] ], 
		# [ [I-Rate],[I-Prob] ], [ [FI-Rate],[FI-Prob] ], [ [P-Rate],[P-Prob] ]
		output=[[[],[]],[[],[]],[[],[]],[[],[]]] 
		for i in range(len(args)):
			with open(args[i]) as csv_file:
			    csv_reader = csv.reader(csv_file, delimiter=',')
			    for row in csv_reader:
			    	output[i][0].append(float(row[0]))
			    	output[i][1].append(float(row[1]))
		return output
	except Exception as e:
		print("Error in probability_CSV",e)
		sys.exit()

"""
#Deprecated
def user_Distribution(pathJSON,fileJSON):
	try:
		# Returns an array containing the percentage of the user 
		# profile with respect to the total users
		# E.g. If TotUsers=100 AND Social=50% ; Streaming=30% ; Business=20% 
		# THEN Social=50 ; Streaming=30 ; Business=20
		f1=open(pathJSON+fileJSON)
		userJson=json.load(f1)
		percent=[[],[],[]]
		percent[0]=int(userJson['social']['distribution'])
		percent[1]=int(userJson['streaming']['distribution'])
		percent[2]=int(userJson['business']['distribution'])
		return percent
	except Exception as e:
		print("Error in user_Distribution",e)
		sys.exit()
"""