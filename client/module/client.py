# FWTG
# Copyright (C) 2022  RomARS
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# 
# The code was developed by Mattia Quadrini <quadrini@romars.tech>
#

import string
import random
import requests
import time
import sys

def makeGET(session,dstIP,dstPort,GETobjsize): 
	# Makes a special GET to http://dstIP:dstPort requesting an object with a size=GETobjsize[kB]
	# Returns the response status and the response time
	try:
		start=time.time()
		get=session.get('http://'+str(dstIP)+':'+str(dstPort)+'/?size='+str(GETobjsize))
		status=get.status_code
		end=time.time()
		deltaT=end-start
		return status, deltaT, GETobjsize
	except Exception as e:
		print("Error in makeGET",e)


def makePOST(session,dstIP,dstPort,POSTobjsize):
	# Makes a special POST to http://dstIP:dstPort requesting an object with a size=POSTobjsize[kB]
	# Returns the response status and the response time
	try:
		#POSTobjsize=2 #kbytes (Uncomment for fixed size)
		onek=''.join(random.choice(string.ascii_lowercase+string.punctuation+string.digits) for x in range(1000))
		msg=''.join(onek for x in range(int(POSTobjsize)))
		start=time.time()
		post=session.post('http://'+str(dstIP)+':'+str(dstPort)+'/',data=msg)
		status=post.status_code
		end=time.time()
		deltaT=end-start
		return status, deltaT, POSTobjsize
	except Exception as e:
		print("Error in makePOST",e)

