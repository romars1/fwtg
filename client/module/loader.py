# FWTG
# Copyright (C) 2022  RomARS
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# 
# The code was developed by Mattia Quadrini <quadrini@romars.tech>
#

from module.config import *
from module.client import *
import numpy as np

serviceCategory=['newspaper','social-network','media-streaming','collaborative']

# Interaction Modes definition
sessionInteractionMode=['non-interactive','interactive','full-interactive',	'play']

# Number of Objects in average per InteractionMode
# 'non-interactive', 'interactive', 'full-interactive'
# 'non-interactive', 'play'
newspaperObject=[366, 340, 5183]
socialNetworkObject=[190, 310, 5121]
mediaStreamingObject=[60, 131]
collaborativeObject=[199, 117, 1024]

## Probabilities to extract a GET/POST per Interaction Mode
# NEWSPAPER
newspaperMethodGet=[97, 97, 95]
newspaperMethodPost=[3,3,5]
# SOCIAL-NETWORK
socialNetworkMethodGet=[91,82,90]
socialNetworkMethodPost=[9,18,10]
# MEDIA-STREAMING
mediaStreamingMethodGet=[93,100]
mediaStreamingMethodPost=[7,0]
# COLLABORATIVE
collaborativeMethodGet=[93,56,8]
collaborativeMethodPost=[7,44,92]


## PERFORMANCE single user
nSession=0
nIM=0
nTotalObjs=0
nGET=0
nPOST=0
nIdle=0

nReqRateGET=[]
nReqRatePOST=[]
sessionTime=[]
nIMTime=[]
nObjSizeGET=[]
nObjSizePOST=[]
nObjTimingGET=[]
nObjTimingPOST=[]
idleTime=[]

avgSessionTime=0
avgIMTime=0
avgObjSizeGET=0
avgObjSizePOST=0
avgObjTimingsGET=0
avgObjTimingsPOST=0
avgIdleTime=0
avgReqRateGET=0
avgReqRatePOST=0


# User Activity
userActivity=user_Activity(jsonPath,jsonUser[0]) # Frequency of activity per profile

# Load user profiles. Probabilities indexes are [i][j]
# i for user profiles = 0:Social, 1:Streaming, 2:Business
# j for service categories =  0:News, 1: Social 2:Streaming 3:Collaborative
userProbabilities=user_Service(jsonPath,jsonUser[0])

# Load ReqRate GET distribution
try:
	newsRateGet=probability_CSV(csvPath+csvNewspaperNI[0],csvPath+csvNewspaperI[0],csvPath+csvNewspaperFI[0])
	socialRateGet=probability_CSV(csvPath+csvSocialNI[0],csvPath+csvSocialI[0],csvPath+csvSocialFI[0])
	streamingRateGet=probability_CSV(csvPath+csvStreamingNI[0],csvPath+csvStreamingP[0])
	collaborativeRateGet=probability_CSV(csvPath+csvCollaborativeNI[0],csvPath+csvCollaborativeI[0],csvPath+csvCollaborativeFI[0])
except Exception as e:
	print(round(time.time(),6),"Error in Load ReqRate GET distribution.",e)
	sys.exit()

# Load ReqRate POST distribution
try:
	newsRatePost=probability_CSV(csvPath+csvNewspaperNI[1],csvPath+csvNewspaperI[1],csvPath+csvNewspaperFI[1])
	socialRatePost=probability_CSV(csvPath+csvSocialNI[1],csvPath+csvSocialI[1],csvPath+csvSocialFI[1])
	streamingRatePost=probability_CSV(csvPath+csvStreamingNI[1],csvPath+csvStreamingP[1])
	collaborativeRatePost=probability_CSV(csvPath+csvCollaborativeNI[1],csvPath+csvCollaborativeI[1],csvPath+csvCollaborativeFI[1])
except Exception as e:
	print(round(time.time(),6),"Error in Load ReqRate POST distribution.",e)
	sys.exit()

try:
	newsSizeGet=probability_CSV(csvPath+csvNewspaperNI[2],csvPath+csvNewspaperI[2],csvPath+csvNewspaperFI[2])
	socialSizeGet=probability_CSV(csvPath+csvSocialNI[2],csvPath+csvSocialI[2],csvPath+csvSocialFI[2])
	streamingSizeGet=probability_CSV(csvPath+csvStreamingNI[2],csvPath+csvStreamingP[2])
	collaborativeSizeGet=probability_CSV(csvPath+csvCollaborativeNI[2],csvPath+csvCollaborativeI[2],csvPath+csvCollaborativeFI[2])
except Exception as e:
	print(round(time.time(),6),"Error in Load Size GET distribution.",e)
	sys.exit()

try:
	newsSizePost=probability_CSV(csvPath+csvNewspaperNI[3],csvPath+csvNewspaperI[3],csvPath+csvNewspaperFI[3])
	socialSizePost=probability_CSV(csvPath+csvSocialNI[3],csvPath+csvSocialI[3],csvPath+csvSocialFI[3])
	streamingSizePost=probability_CSV(csvPath+csvStreamingNI[3],csvPath+csvStreamingP[3])
	collaborativeSizePost=probability_CSV(csvPath+csvCollaborativeNI[3],csvPath+csvCollaborativeI[3],csvPath+csvCollaborativeFI[3])
except Exception as e:
	print(round(time.time(),6),"Error in Load Size POST distribution.",e)
	sys.exit()


def select_imObj(_ServiceCategory, _InteractionMode):
	# Select the number of total object to be requested depending 
	# on the selected service category and interaction mode
	try:
		# NEWSPAPER
		if (_ServiceCategory==serviceCategory[0]):
			if (_InteractionMode==sessionInteractionMode[0]):
				nObjects=newspaperObject[0]
			elif (_InteractionMode==sessionInteractionMode[1]):
				nObjects=newspaperObject[1]
			elif (_InteractionMode==sessionInteractionMode[2]):
				nObjects=newspaperObject[2]
		
		# SOCIAL-MNETWORK
		elif (_ServiceCategory==serviceCategory[1]):
			if (_InteractionMode==sessionInteractionMode[0]):
				nObjects=socialNetworkObject[0]
			elif (_InteractionMode==sessionInteractionMode[1]):
				nObjects=socialNetworkObject[1]
			elif (_InteractionMode==sessionInteractionMode[2]):
				nObjects=socialNetworkObject[2]
		
		# MEDIA-STREAMING
		elif (_ServiceCategory==serviceCategory[2]):
			if (_InteractionMode==sessionInteractionMode[0]):
				nObjects=mediaStreamingObject[0]
			elif (_InteractionMode==sessionInteractionMode[3]):
				nObjects=mediaStreamingObject[1]
		
		# COLLABORATIVE
		elif (_ServiceCategory==serviceCategory[3]):
			if (_InteractionMode==sessionInteractionMode[0]):
				nObjects=collaborativeObject[0]
			elif (_InteractionMode==sessionInteractionMode[1]):
				nObjects=collaborativeObject[1]
			elif (_InteractionMode==sessionInteractionMode[2]):
				nObjects=collaborativeObject[2]
		return nObjects
	except Exception as e:
		print(round(time.time(),6),"Error in in select_imObj.",e)
		sys.exit()

def select_Method(_InteractionMode, _ServiceCategory):
	try:
		# Newspaper
		if (_ServiceCategory==serviceCategory[0]): 
			if (_InteractionMode==sessionInteractionMode[0]):
				rand_method=str(random.choices(['GET', 'POST'],weights=[newspaperMethodGet[0],newspaperMethodPost[0]], k=1)) # NI
			elif (_InteractionMode==sessionInteractionMode[1]):
				rand_method=str(random.choices(['GET', 'POST'],weights=[newspaperMethodGet[1],newspaperMethodPost[1]], k=1)) # I
			elif (_InteractionMode==sessionInteractionMode[2]):
				rand_method=str(random.choices(['GET', 'POST'],weights=[newspaperMethodGet[2],newspaperMethodPost[2]], k=1)) # FI
		
		# Social-Network
		if (_ServiceCategory==serviceCategory[1]): 
			if (_InteractionMode==sessionInteractionMode[0]):
				rand_method=str(random.choices(['GET', 'POST'],weights=[socialNetworkMethodGet[0],socialNetworkMethodPost[0]], k=1)) # NI
			elif (_InteractionMode==sessionInteractionMode[1]):
				rand_method=str(random.choices(['GET', 'POST'],weights=[socialNetworkMethodGet[1],socialNetworkMethodPost[1]], k=1)) # I
			elif (_InteractionMode==sessionInteractionMode[2]):
				rand_method=str(random.choices(['GET', 'POST'],weights=[socialNetworkMethodGet[2],socialNetworkMethodPost[2]], k=1)) # FI
		
		# Media-Streaming
		if (_ServiceCategory==serviceCategory[2]): 
			if (_InteractionMode==sessionInteractionMode[0]):
				rand_method=str(random.choices(['GET', 'POST'],weights=[mediaStreamingMethodGet[0],mediaStreamingMethodPost[0]], k=1)) # NI
			elif (_InteractionMode==sessionInteractionMode[3]):
				rand_method=str(random.choices(['GET', 'POST'],weights=[mediaStreamingMethodGet[1],mediaStreamingMethodPost[1]], k=1)) # P
		
		# Collaborative
		if (_ServiceCategory==serviceCategory[3]): 
			if (_InteractionMode==sessionInteractionMode[0]):
				rand_method=str(random.choices(['GET', 'POST'],weights=[collaborativeMethodGet[0],collaborativeMethodPost[0]], k=1)) # NI
			elif (_InteractionMode==sessionInteractionMode[1]):
				rand_method=str(random.choices(['GET', 'POST'],weights=[collaborativeMethodGet[1],collaborativeMethodPost[1]], k=1)) # I
			elif (_InteractionMode==sessionInteractionMode[2]):
				rand_method=str(random.choices(['GET', 'POST'],weights=[collaborativeMethodGet[2],collaborativeMethodPost[2]], k=1)) # FI		
		
		return rand_method[2:len(rand_method)-2]
	except Exception as e:
		print(round(time.time(),6),"Error in select_Method.",e)
		sys.exit()


def select_ReqRate(_Session,_InteractionMode,_Method):
	try:
		# Depending on the Category/Interaction Mode/Method select reqRate
		# NEWS-PAPER
		if(_Session.category==serviceCategory[0] and _InteractionMode==sessionInteractionMode[0] and _Method=='GET'):
			x=random.choices(newsRateGet[0][0],weights=newsRateGet[0][1],k=1)
		elif(_Session.category==serviceCategory[0] and _InteractionMode==sessionInteractionMode[0] and _Method=='POST'):
			x=random.choices(newsRatePost[0][0],weights=newsRatePost[0][1],k=1)

		elif(_Session.category==serviceCategory[0] and _InteractionMode==sessionInteractionMode[1] and _Method=='GET'):
			x=random.choices(newsRateGet[1][0],weights=newsRateGet[1][1],k=1)
		elif(_Session.category==serviceCategory[0] and _InteractionMode==sessionInteractionMode[1] and _Method=='POST'):
			x=random.choices(newsRatePost[1][0],weights=newsRatePost[1][1],k=1)

		elif(_Session.category==serviceCategory[0] and _InteractionMode==sessionInteractionMode[2] and _Method=='GET'):
			x=random.choices(newsRateGet[2][0],weights=newsRateGet[2][1],k=1)
		elif(_Session.category==serviceCategory[0] and _InteractionMode==sessionInteractionMode[2] and _Method=='POST'):
			x=random.choices(newsRatePost[2][0],weights=newsRatePost[2][1],k=1)
			
		# SOCIAL-MEDIA
		elif(_Session.category==serviceCategory[1] and _InteractionMode==sessionInteractionMode[0] and _Method=='GET'):
			x=random.choices(socialRateGet[0][0],weights=socialRateGet[0][1],k=1)
		elif(_Session.category==serviceCategory[1] and _InteractionMode==sessionInteractionMode[0] and _Method=='POST'):
			x=random.choices(socialRatePost[0][0],weights=socialRatePost[0][1],k=1)

		elif(_Session.category==serviceCategory[1] and _InteractionMode==sessionInteractionMode[1] and _Method=='GET'):
			x=random.choices(socialRateGet[1][0],weights=socialRateGet[1][1],k=1)
		elif(_Session.category==serviceCategory[1] and _InteractionMode==sessionInteractionMode[1] and _Method=='POST'):
			x=random.choices(socialRatePost[1][0],weights=socialRatePost[1][1],k=1)

		elif(_Session.category==serviceCategory[1] and _InteractionMode==sessionInteractionMode[2] and _Method=='GET'):
			x=random.choices(socialRateGet[2][0],weights=socialRateGet[2][1],k=1)
		elif(_Session.category==serviceCategory[1] and _InteractionMode==sessionInteractionMode[2] and _Method=='POST'):
			x=random.choices(socialRatePost[2][0],weights=socialRatePost[2][1],k=1)
			
		# MEDIA-STREAMING
		elif(_Session.category==serviceCategory[2] and _InteractionMode==sessionInteractionMode[0] and _Method=='GET'):
			x=random.choices(streamingRateGet[0][0],	weights=streamingRateGet[0][1],k=1)
		elif(_Session.category==serviceCategory[2] and _InteractionMode==sessionInteractionMode[0] and _Method=='POST'):
			x=random.choices(streamingRatePost[0][0],weights=streamingRatePost[0][1],k=1)

		elif(_Session.category==serviceCategory[2] and _InteractionMode==sessionInteractionMode[3] and _Method=='GET'):
			x=random.choices(streamingRateGet[1][0],weights=streamingRateGet[1][1],k=1)
		elif(_Session.category==serviceCategory[2] and _InteractionMode==sessionInteractionMode[3] and _Method=='POST'):
			x=random.choices(streamingRatePost[1][0],weights=streamingRatePost[1][1],k=1)
			
		# COLLABORATIVE
		elif(_Session.category==serviceCategory[3] and _InteractionMode==sessionInteractionMode[0] and _Method=='GET'):
			x=random.choices(collaborativeRateGet[0][0],weights=collaborativeRateGet[0][1],k=1)
		elif(_Session.category==serviceCategory[3] and _InteractionMode==sessionInteractionMode[0] and _Method=='POST'):
			x=random.choices(collaborativeRatePost[0][0],weights=collaborativeRatePost[0][1],k=1)

		elif(_Session.category==serviceCategory[3] and _InteractionMode==sessionInteractionMode[1] and _Method=='GET'):
			x=random.choices(collaborativeRateGet[1][0],weights=collaborativeRateGet[1][1],k=1)
		elif(_Session.category==serviceCategory[3] and _InteractionMode==sessionInteractionMode[1] and _Method=='POST'):
			x=random.choices(collaborativeRatePost[1][0],weights=collaborativeRatePost[1][1],k=1)

		elif(_Session.category==serviceCategory[3] and _InteractionMode==sessionInteractionMode[2] and _Method=='GET'):
			x=random.choices(collaborativeRateGet[2][0],weights=collaborativeRateGet[2][1],k=1)
		elif(_Session.category==serviceCategory[3] and _InteractionMode==sessionInteractionMode[2] and _Method=='POST'):
			x=random.choices(collaborativeRatePost[2][0],weights=collaborativeRatePost[2][1],k=1)

		return int(x[0])
	except Exception as e:
		print(round(time.time(),6),"Error in select_ReqRate.",e)
		sys.exit()

def select_Size(_Session,_InteractionMode,_Method):
	try:
		# Depending on the Category/Interaction Mode/_Method select reqSize
		# NEWS-PAPER
		if(_Session.category==serviceCategory[0] and _InteractionMode==sessionInteractionMode[0] and _Method=='GET'):
			x=random.choices(newsSizeGet[0][0],weights=newsSizeGet[0][1],k=1)

		elif(_Session.category==serviceCategory[0] and _InteractionMode==sessionInteractionMode[0] and _Method=='POST'):
			x=random.choices(newsSizePost[0][0],weights=newsSizePost[0][1],k=1)

		elif(_Session.category==serviceCategory[0] and _InteractionMode==sessionInteractionMode[1] and _Method=='GET'):
			x=random.choices(newsSizeGet[1][0],weights=newsSizeGet[1][1],k=1)

		elif(_Session.category==serviceCategory[0] and _InteractionMode==sessionInteractionMode[1] and _Method=='POST'):
			x=random.choices(newsSizePost[1][0],weights=newsSizePost[1][1],k=1)

		elif(_Session.category==serviceCategory[0] and _InteractionMode==sessionInteractionMode[2] and _Method=='GET'):
			x=random.choices(newsSizeGet[2][0],weights=newsSizeGet[2][1],k=1)
			
		elif(_Session.category==serviceCategory[0] and _InteractionMode==sessionInteractionMode[2] and _Method=='POST'):
			x=random.choices(newsSizePost[2][0],weights=newsSizePost[2][1],k=1)
			
		# SOCIAL-MEDIA
		elif(_Session.category==serviceCategory[1] and _InteractionMode==sessionInteractionMode[0] and _Method=='GET'):
			x=random.choices(socialSizeGet[0][0],weights=socialSizeGet[0][1],k=1)
			
		elif(_Session.category==serviceCategory[1] and _InteractionMode==sessionInteractionMode[0] and _Method=='POST'):
			x=random.choices(socialSizePost[0][0],weights=socialSizePost[0][1],k=1)
			
		elif(_Session.category==serviceCategory[1] and _InteractionMode==sessionInteractionMode[1] and _Method=='GET'):
			x=random.choices(socialSizeGet[1][0],weights=socialSizeGet[1][1],k=1)
			
		elif(_Session.category==serviceCategory[1] and _InteractionMode==sessionInteractionMode[1] and _Method=='POST'):
			x=random.choices(socialSizePost[1][0],weights=socialSizePost[1][1],k=1)
			
		elif(_Session.category==serviceCategory[1] and _InteractionMode==sessionInteractionMode[2] and _Method=='GET'):
			x=random.choices(socialSizeGet[2][0],weights=socialSizeGet[2][1],k=1)
			
		elif(_Session.category==serviceCategory[1] and _InteractionMode==sessionInteractionMode[2] and _Method=='POST'):
			x=random.choices(socialSizePost[2][0],weights=socialSizePost[2][1],k=1)
			
		# MEDIA-STREAMING
		elif(_Session.category==serviceCategory[2] and _InteractionMode==sessionInteractionMode[0] and _Method=='GET'):
			x=random.choices(streamingSizeGet[0][0],	weights=streamingSizeGet[0][1],k=1)
			
		elif(_Session.category==serviceCategory[2] and _InteractionMode==sessionInteractionMode[0] and _Method=='POST'):
			x=random.choices(streamingSizePost[0][0],weights=streamingSizePost[0][1],k=1)
			
		elif(_Session.category==serviceCategory[2] and _InteractionMode==sessionInteractionMode[3] and _Method=='GET'):
			x=random.choices(streamingSizeGet[1][0],weights=streamingSizeGet[1][1],k=1)
			
		elif(_Session.category==serviceCategory[2] and _InteractionMode==sessionInteractionMode[3] and _Method=='POST'):
			x=random.choices(streamingSizePost[1][0],weights=streamingSizePost[1][1],k=1)
			
		# COLLABORATIVE
		elif(_Session.category==serviceCategory[3] and _InteractionMode==sessionInteractionMode[0] and _Method=='GET'):
			x=random.choices(collaborativeSizeGet[0][0],weights=collaborativeSizeGet[0][1],k=1)
			
		elif(_Session.category==serviceCategory[3] and _InteractionMode==sessionInteractionMode[0] and _Method=='POST'):
			x=random.choices(collaborativeSizePost[0][0],weights=collaborativeSizePost[0][1],k=1)
			
		elif(_Session.category==serviceCategory[3] and _InteractionMode==sessionInteractionMode[1] and _Method=='GET'):
			x=random.choices(collaborativeSizeGet[1][0],weights=collaborativeSizeGet[1][1],k=1)
			
		elif(_Session.category==serviceCategory[3] and _InteractionMode==sessionInteractionMode[1] and _Method=='POST'):
			x=random.choices(collaborativeSizePost[1][0],weights=collaborativeSizePost[1][1],k=1)
			
		elif(_Session.category==serviceCategory[3] and _InteractionMode==sessionInteractionMode[2] and _Method=='GET'):
			x=random.choices(collaborativeSizeGet[2][0],weights=collaborativeSizeGet[2][1],k=1)
			
		elif(_Session.category==serviceCategory[3] and _InteractionMode==sessionInteractionMode[2] and _Method=='POST'):
			x=random.choices(collaborativeSizePost[2][0],weights=collaborativeSizePost[2][1],k=1)
		return int(x[0])
	except Exception as e:
		print(round(time.time(),6),"Error",e)
		sys.exit()


#def stop_Emulation(_StartTime,_StopTime,_log,_conn):
def stop_Emulation(_StartTime,_StopTime,_conn):
	try:
		time.sleep(_StopTime)
		#print("–",round(time.time()-_StartTime,6)," The emulation is terminated: ",str(round(time.time()-_StartTime,3))+"\n",file=_log)
		print("–",round(time.time()-_StartTime,6)," The emulation is terminated: ",str(round(time.time()-_StartTime,3))+"\n")
		#print("-------------------------------------------------",file=_log)
		#print("]                 STATISTICS                     ",file=_log)
		#print("-------------------------------------------------",file=_log)
		#print("] Number of Session:",nSession,file=_log)
		#print("] Number of IM:",nIM,file=_log)
		#print("] Number of Idles:",nIdle,file=_log)
		#print("] Number of Requested Objects:",nTotalObjs,file=_log)
		#print("]",file=_log)
		#print("] Number of GET:",nGET,str(math.floor((nGET/nTotalObjs)*100))+"%",file=_log)
		#print("] Number of POST:",nPOST,str(math.floor((nPOST/nTotalObjs)*100))+"%",file=_log)
		#print("]",file=_log)
		#print("] Average Session Time:",round(np.mean(sessionTime),3),file=_log)
		#print("] Average IM Time:",round(np.mean(nIMTime),3),file=_log)
		#print("] Average Idle Time:",round(np.mean(idleTime),3),file=_log)
		#print("]",file=_log)
		#print("] Average Request Rate GET:",round(np.mean(nReqRateGET),3),file=_log)
		#print("] Average Request Rate POST:",round(np.mean(nReqRatePOST),3),file=_log)
		#print("] Average Object Size GET:",round(np.mean(nObjSizeGET),3),"kB",file=_log)
		#print("] Average Object Size POST:",round(np.mean(nObjSizePOST),3),"kB",file=_log)
		#print("] Average Object Timings GET:",round(np.mean(nObjTimingGET),3),file=_log)
		#print("] Average Object Timings POST:",round(np.mean(nObjTimingPOST),3),file=_log)
		#print("-------------------------------------------------",file=_log)
		print("-------------------------------------------------")
		print("]                 STATISTICS                     ")
		print("-------------------------------------------------")
		print("] Number of Session:",nSession)
		print("] Number of IM:",nIM)
		print("] Number of Idles:",nIdle)
		print("] Number of Requested Objects:",nTotalObjs)
		print("]")
		print("] Number of GET:",nGET,str(math.floor((nGET/nTotalObjs)*100))+"%")
		print("] Number of POST:",nPOST,str(math.floor((nPOST/nTotalObjs)*100))+"%")
		print("]")
		print("] Average Session Time:",round(np.mean(sessionTime),3))
		print("] Average IM Time:",round(np.mean(nIMTime),3))
		print("] Average Idle Time:",round(np.mean(idleTime),3))
		print("]")
		print("] Average Request Rate GET:",round(np.mean(nReqRateGET),3))
		print("] Average Request Rate POST:",round(np.mean(nReqRatePOST),3))
		print("] Average Object Size GET:",round(np.mean(nObjSizeGET),3),"kB")
		print("] Average Object Size POST:",round(np.mean(nObjSizePOST),3),"kB")
		print("] Average Object Timings GET:",round(np.mean(nObjTimingGET),3))
		print("] Average Object Timings POST:",round(np.mean(nObjTimingPOST),3))
		print("-------------------------------------------------")
		#_log.close()
		print("Cleaning...")
		for c in _conn:
			c.close()
		# Send CTRL+C command to the process with PID
		os.kill(os.getpid(),signal.SIGINT) 
		#signal.SIGINT # Send CTRL+C command
	except Exception as e:
		print(round(time.time()-_StartTime,6),'Error in stop_Emulation.',e)

def run_Emulation(_profile,_emulationTime,_dstIP,_dstPort,_verbose):
	global nSession
	global nIM
	global nTotalObjs
	global nGET
	global nPOST
	global nIdle
	global nReqRateGET
	global nReqRatePOST
	global sessionTime
	global nIMTime
	global nObjSizeGET
	global nObjTimingGET
	global nObjSizePOST
	global nObjTimingPOST
	global idleTime
	try:
		# Selecting Profile
		if (_profile=='social'):
			u=User('social')
			u.add_activity(userActivity[0])
			u.add_service(userProbabilities[0])
		elif (_profile=='streaming'):
			u=User('streaming')
			u.add_activity(userActivity[1])
			u.add_service(userProbabilities[1])
		elif (_profile=='business'):
			u=User('business')
			u.add_activity(userActivity[2])
			u.add_service(userProbabilities[2])
		elif (_profile=='custom'):
			u=User('custom')
			u.add_activity(userActivity[3])
			u.add_service(userProbabilities[3])
		# Create log file in ./log
		userIndex=1
		#name='log-user-'+str(_profile)+'-'+str(userIndex)+'.txt'
		#if not ('log' in os.listdir()):
		#	os.mkdir(os.path.abspath('log'))
		#while (name in os.listdir(os.path.abspath('./log'))):
		#	userIndex=userIndex+1
		#	name='log-user-'+str(_profile)+'-'+str(userIndex)+'.txt'
		#log=open('./log/'+name,'w')
		# Number of Maximum Parallel Requests per Session
		maxParReq=4 
		# Number of maximum connections per Session
		maxConn=6 
		# Fix startTime
		connections=[]
		emulationStart=time.time()
		#print("–",round(time.time()-emulationStart),"Emulation Started",file=log)
		print("–",round(time.time()-emulationStart),"Emulation Started")
		# Recall stop_Emulation
		#th=threading.Thread(target=stop_Emulation, args=(emulationStart,_emulationTime,log,connections))
		th=threading.Thread(target=stop_Emulation, args=(emulationStart,_emulationTime,connections))
		th.start()
		# Create Session duration vector
		activityTime=_emulationTime*u.activity/100
		sessionTime=create_Session(_emulationTime, activityTime)
		nSession=len(sessionTime)
		sessionTimeIndex=0
		# Evaluate total time of Idle
		totalIdleTime=_emulationTime-activityTime
		# Create Idle vector
		idleTime=create_Idle(totalIdleTime)
		nIdle=len(idleTime)
		idleTimeIndex=0
		emulationVector=[]
		emulationIndex=0
		for v in range(0,len(sessionTime)):
			emulationVector.append("S")
		for v in range(0,len(idleTime)):
			emulationVector.append("I")
		random.shuffle(emulationVector)
		random.shuffle(emulationVector)
		print("] Events:", emulationVector)
		#print("] Events:", emulationVector,file=log)
		print("] Session Times:", sessionTime)
		#print("] Session Times:", sessionTime,file=log)
		print("] Idle Times:", str(idleTime))
		#print("] Idle Times:", str(idleTime),file=log)
		print("] User Activity: ["+str(u.activity)+"]")
		#print("] User Activity: ["+str(u.activity)+"]",file=log)
		print("] User Profile:",str(u.service))
		#print("] User Profile:",str(u.service),file=log)
		print("] ")
		#print("] ",file=log)

	except Exception as e:
		print("|",round(time.time()-emulationStart,6),_profile,userIndex,'Error in run_Emulation',e)
		#print("|",round(time.time()-emulationStart,6),_profile,userIndex,'Error in run_Emulation',e,file=log)
		signal.SIGINT
		sys.exit()	
		# Effetive start of the program
	try:
		while (True):
		# START of While-Main: run emulation according to the emulationVector
			if (emulationIndex >= len(emulationVector)):
				break
			if (emulationVector[emulationIndex] == 'I'):
				idle=idleTime[idleTimeIndex]
				#print("|",round(time.time()-emulationStart,6),_profile,userIndex,'IDLE ('+str(idle)+')',file=log)
				if (_verbose): print("|",round(time.time()-emulationStart,6),_profile,userIndex,'IDLE ('+str(idle)+')')
				for t in range(1,int(idle)+1):
					if (time.time() >= emulationStart+_emulationTime): break 
					time.sleep(1)
					#print(t,end="\r",flush=True) #DEBUG
				#time.sleep(idle)
				idleTimeIndex=idleTimeIndex+1
				emulationIndex=emulationIndex+1
			elif (emulationVector[emulationIndex] == 'S'):
				#print("|",round(time.time()-emulationStart,6),_profile,userIndex,'New Session',file=log)
				if (_verbose): print("|",round(time.time()-emulationStart,6),_profile,userIndex,'New Session')
				try:
					# Create one requests.Session for each connection. 
					# The first object using that connection will trigger TCP handshake
					for i in range(0,maxConn):
						connections.append(requests.Session())
					# First interaction mode of a new session will be always non-interactive
					im='' 
					# Extract a random Session category and duration
					sCategory=random_Category(u,serviceCategory)
					# Create the User Session. The duration is taken by the vector
					s=Session(sCategory, sessionTime[sessionTimeIndex])
					sessionTimeIndex=sessionTimeIndex+1
					emulationIndex=emulationIndex+1
					# Fix the time to terminate the User Session
					sessionNow=time.time() 
				except Exception as e:
					#print('Error creating a Session',e,file=log)
					print('Error creating a Session',e)
				# If Category = Media-Streaming
				if (s.category==serviceCategory[2]):
					# MEDIA STREAMING model
					# delta_T=10s
					# obj_size_max=2MB
					# obj_size_min=1.5MB
					#print("|",round(time.time()-emulationStart,6),_profile,userIndex,'Media Streaming started',file=log)
					if (_verbose): print("|",round(time.time()-emulationStart,6),_profile,userIndex,'Media Streaming started')
					while (time.time()<sessionNow+s.duration):
						connReq=connections[0]
						objSize=random.choices([1500,1750,2000],weights=[1,1,1],k=1)[0]
						[status,rtt,obj]=makeGET(connReq,_dstIP,_dstPort,objSize)
						nObjSizeGET.append(obj) #LOG
						nObjTimingGET.append(rtt) #LOG
						nTotalObjs=nTotalObjs+1 #LOG
						nGET=nGET+1 #LOG
						#print("|",round(time.time()-emulationStart,6),_profile,userIndex,'GET',status,'request-rate','1','size',str(obj),'timing',str(round(rtt,4)),file=log)
						if (_verbose): print("|",round(time.time()-emulationStart,6),_profile,userIndex,'GET',status,'request-rate','1','size',str(obj),'timing',str(round(rtt,4)))
						if (time.time()>sessionNow+s.duration+10-rtt):
							break
						else:
							time.sleep(10-int(rtt%10))
				else:
					while (time.time()<sessionNow+s.duration):
						try:
							# START of While-Session: create a new InteractionMode
							#print("|",round(time.time()-emulationStart,6),_profile,userIndex,'New Interaction Mode',file=log)
							if (_verbose): print("|",round(time.time()-emulationStart,6),_profile,userIndex,'New Interaction Mode')
							# If IM = '' then select NI
							if (im==''):
								im=sessionInteractionMode[0]
							# If IM = NI then select NI with a p=0.3 or I with a p=0.7
							elif (im==sessionInteractionMode[0]):
								im=str(random.choices([sessionInteractionMode[0], sessionInteractionMode[1]],weights=[30,70],k=1)[0])
							# If IM = I then select I with a p=0.3 or FI with a p=0.7
							elif (im==sessionInteractionMode[1]):
								im=str(random.choices([sessionInteractionMode[1], sessionInteractionMode[2]],weights=[30,70],k=1)[0])
							# If IM = FI then select FI
							elif (im==sessionInteractionMode[2]):
								im=sessionInteractionMode[2]
							nIM=nIM+1 #LOG
							# Initialize the number of downloaded objects
							nObj=0
							# Fix the number of objects to request depending on the User Session and IM 
							# The IM is completed when all objects are downloaded
							imObj=select_imObj(s.category,im);
							imStart=time.time()
							while (nObj<imObj):
							# START of While-InteractionMode: create a pool of requests
								# Check if session time expired
								if (time.time()>sessionNow+s.duration): 
									break
								# First request of an NI is a GET
								if (im==sessionInteractionMode[0]):
									method='GET'
								else:
									# Select the Method: GET/POST
									method=select_Method(im,s.category) 
								if (method=='GET'):
									# Extract the ReqRate value
									reqRate=select_ReqRate(s,im,method)
									nReqRateGET.append(reqRate) #LOG
									# A ReqRate = 0 means a waiting time of 1s
									if (reqRate==0):
										nObjSizeGET.append(0) #LOG
										nObjTimingGET.append(1) #LOG
										status0='0'
										#print("|",round(time.time()-emulationStart,6),_profile,userIndex,method,status0,'request-rate','0','size','0','timing','1',file=log)
										if (_verbose): print("|",round(time.time()-emulationStart,6),_profile,userIndex,method,status0,'request-rate','0','size','0','timing','1')
										time.sleep(1)
										continue
									else:
										#print("|",round(time.time()-emulationStart,6),_profile,userIndex,method,'req-rate['+str(reqRate)+']','service-category['+str(s.category)+']','mode['+str(im)+']','objects['+str(nObj)+'/'+str(imObj)+']','session-time['+str(s.duration)+'s]',file=log)
										if (_verbose): print("|",round(time.time()-emulationStart,6),_profile,userIndex,method,'req-rate['+str(reqRate)+']','service-category['+str(s.category)+']','mode['+str(im)+']','objects['+str(nObj)+'/'+str(imObj)+']','session-time['+str(s.duration)+'s]')
									try:
										# Fix the number of parallel requests in a connection to do
										parReqArrayGET=[]
										if (reqRate<=maxParReq):
											parReq=int(reqRate)
										elif (reqRate>maxParReq):
											parReq=int(random.randrange(2,maxParReq,1)) # Startint from 2 because range(0,2) will do 2 times
											#print("|",round(time.time()-emulationStart,6),_profile,userIndex,method,str(parReq),'Parallel Requests',file=log)
											if (_verbose): print("|",round(time.time()-emulationStart,6),_profile,userIndex,method,str(parReq),'Parallel Requests')
										executor=concurrent.futures.ThreadPoolExecutor(max_workers=parReq)
										# First X requests are done in parallel in the connection 0
										for par in range(0,parReq):
											#if (time.time()>emulationStart+_emulationTime):
											#	break
											# Every For cycle check is IM is completed = all objects have been downloaded
											if (nObj>=imObj): 
												break
											# Extract object size
											objSize=select_Size(s,im,method)
											nObjSizeGET.append(objSize) #LOG
											# First connection (0) is selected
											connReq=connections[0]
											# Do first X requests
											parReqArrayGET.append(executor.submit(makeGET,connReq,_dstIP,_dstPort,objSize))
											# Update number of requested objects every request (= for cycle)
											nObj=nObj+1
											nTotalObjs=nTotalObjs+1 #LOG
											nGET=nGET+1 #LOG
											time.sleep(1/reqRate)
										# Print the output all at once
										for req in parReqArrayGET: 
											status=req.result()[0]
											rtt=req.result()[1]
											obj=req.result()[2]
											nObjTimingGET.append(rtt) #LOG
											#print("|",round(time.time()-emulationStart,6),_profile,userIndex,method,status,'request-rate',str(reqRate),'size',str(obj),'timing',str(round(rtt,4)),file=log)
											if (_verbose): print("|",round(time.time()-emulationStart,6),_profile,userIndex,method,status,'request-rate',str(reqRate),'size',str(obj),'timing',str(round(rtt,4)))
										# Because of parallel requests, then wait for 1/ReqRate 
										# Handle non-parallel requests
										parConnArrayGET=[] 
										# Check if there are remaining requests. If Parallel Requests = Request Rate then 
										# all requests have been completed at the previous stage
										if (reqRate-parReq>0):
											executor=concurrent.futures.ThreadPoolExecutor(max_workers=reqRate-parReq)
										else:
											continue
										for r in range(0,reqRate-parReq):
											# Every For cycle check is IM is completed
											#if (time.time()>emulationStart+_emulationTime):
											#	break
											if (nObj>=imObj):
												break
											# Extract object size
											objSize=select_Size(s,im,method)
											nObjSizeGET.append(objSize) #LOG
											# Select the connection to be used. 
											# It will select in loop [0,1,2,3...,0,1,2,3...] (RoundRobin)
											connReq=connections[r%maxConn]
											parConnArrayGET.append(executor.submit(makeGET,connReq,_dstIP,_dstPort,objSize))
											nObj=nObj+1
											nTotalObjs=nTotalObjs+1 #LOG
											nGET=nGET+1 #LOG
											time.sleep(1/reqRate)
										# Print output all at once
										for conn in parConnArrayGET: # Print output at once
											status=conn.result()[0]
											rtt=conn.result()[1]
											obj=conn.result()[2]
											nObjTimingGET.append(rtt) #LOG
											#print("|",round(time.time()-emulationStart,6),_profile,userIndex,method,status,'request-rate',str(reqRate),'size',str(obj),'timing',str(round(rtt,4)),file=log)
											if (_verbose): print("|",round(time.time()-emulationStart,6),_profile,userIndex,method,status,'request-rate',str(reqRate),'size',str(obj),'timing',str(round(rtt,4)))
									except Exception as e:
										print("|",round(time.time()-emulationStart,6),_profile,userIndex,'Error in GET Request.',e)
										#print("|",round(time.time()-emulationStart,6),_profile,userIndex,'Error in GET Request.',e,file=log)
										#sys.exit()
								# Same logic for POST method
								elif (method=='POST'):
									reqRate=select_ReqRate(s,im,method)
									nReqRatePOST.append(reqRate) #LOG
									if (reqRate==0):
										nObjSizePOST.append(0) #LOG
										nObjTimingPOST.append(1) #LOG
										status0='0'
										#print("|",round(time.time()-emulationStart,6),_profile,userIndex,method,status0,'request-rate','0','size','0','timing','1',file=log)
										if (_verbose): print("|",round(time.time()-emulationStart,6),_profile,userIndex,method,status0,'request-rate','0','size','0','timing','1')
										time.sleep(1)
										continue
									else:
										#print("|",round(time.time()-emulationStart,6),_profile,userIndex,method,'req-rate['+str(reqRate)+']','service-category['+str(s.category)+']','mode['+str(im)+']','objects['+str(nObj)+'/'+str(imObj)+']','session-time['+str(s.duration)+'s]',file=log)
										if (_verbose): print("|",round(time.time()-emulationStart,6),_profile,userIndex,method,'req-rate['+str(reqRate)+']','service-category['+str(s.category)+']','mode['+str(im)+']','objects['+str(nObj)+'/'+str(imObj)+']','session-time['+str(s.duration)+'s]')
									try:
										parReqArrayPOST=[] 
										if (reqRate<=maxParReq):
											parReq=int(reqRate)
										elif (reqRate>maxParReq):
											parReq=int(random.randrange(2,maxParReq,1)) 
										#print("|",round(time.time()-emulationStart,6),_profile,userIndex,method,str(parReq),'Parallel Requests',file=log)
										if (_verbose): print("|",round(time.time()-emulationStart,6),_profile,userIndex,method,str(parReq),'Parallel Requests')
										executor=concurrent.futures.ThreadPoolExecutor(max_workers=parReq)
										for par in range(0,parReq): 
											#if (time.time()>emulationStart+_emulationTime):
											#	break
											if (nObj>=imObj): 
												break
											objSize=select_Size(s,im,method)
											nObjSizePOST.append(objSize) #LOG
											connReq=connections[0]
											parReqArrayPOST.append(executor.submit(makePOST,connReq,_dstIP,_dstPort,objSize))
											nObj=nObj+1
											nTotalObjs=nTotalObjs+1 #LOG
											nPOST=nPOST+1 #LOG
											time.sleep(1/reqRate)
										for req in parReqArrayPOST: 
											status=req.result()[0]
											rtt=req.result()[1]
											obj=req.result()[2]
											nObjTimingPOST.append(rtt) #LOG
											#print("|",round(time.time()-emulationStart,6),_profile,userIndex,method,status,'request-rate',str(reqRate),'size',str(obj),'timing',str(round(rtt,4)),file=log)
											if (_verbose): print("|",round(time.time()-emulationStart,6),_profile,userIndex,method,status,'request-rate',str(reqRate),'size',str(obj),'timing',str(round(rtt,4))) 
										parConnArrayPOST=[] 
										if (reqRate-parReq>0):
											executor=concurrent.futures.ThreadPoolExecutor(max_workers=reqRate-parReq) 
										else:
											continue
										# Handle non-parallel requests
										for r in range(0,reqRate-parReq):
											#if (time.time()>emulationStart+_emulationTime):
											#	break
											if (nObj>=imObj):
												break
											objSize=select_Size(s,im,method)
											nObjSizePOST.append(objSize) #LOG
											connReq=connections[r%maxConn] 
											parConnArrayPOST.append(executor.submit(makePOST,connReq,_dstIP,_dstPort,objSize))
											nObj=nObj+1
											nTotalObjs=nTotalObjs+1 #LOG
											nPOST=nPOST+1 #LOG
											time.sleep(1/reqRate)
										for conn in parConnArrayPOST: 
											status=conn.result()[0]
											rtt=conn.result()[1]
											obj=conn.result()[2]
											nObjTimingPOST.append(rtt) #LOG
											#print("|",round(time.time()-emulationStart,6),_profile,userIndex,method,status,'request-rate',str(reqRate),'size',str(obj),'timing',str(round(rtt,4)),file=log)
											if (_verbose): print("|",round(time.time()-emulationStart,6),_profile,userIndex,method,status,'request-rate',str(reqRate),'size',str(obj),'timing',str(round(rtt,4)))						
									except Exception as e:
										print("|",round(time.time()-emulationStart,6),_profile,userIndex,'Error in POST Request.',e)
										#print("|",round(time.time()-emulationStart,6),_profile,userIndex,'Error in POST Request.',e,file=log)
										#sys.exit()
								# END of the cycle in While-InteractionMode.
							# Put here the code to execute after the While-InteractionMode in the While-Session
							imEnd=time.time()
							imTime=imEnd-imStart
							nIMTime.append(imTime) #LOG
							#print("|",round(time.time()-emulationStart,6),_profile,userIndex,'Interaction Mode completed ('+str(round(imTime,3))+'s).',file=log)
							if (_verbose): print("|",round(time.time()-emulationStart,6),_profile,userIndex,'Interaction Mode completed ('+str(round(imTime,3))+'s).')
							# Check if session time expired
							if (time.time()>sessionNow+s.duration):
									break
							# END of the While-Session.
						except Exception as e:
							print("Error in While-Session",e)
							#print("Error in While-Session",e,file=log)
			# Put here the code to execute after the While-Session in the While-Main
		# END Actions in While-Main
		print("End of Emulation")
		#print("End of Emulation",file=log)
	except Exception as e:
		#print ("Error in While-Main", e,file=log)
		print ("Error in While-Main", e)
	except KeyboardInterrupt:
		sys.exit()
