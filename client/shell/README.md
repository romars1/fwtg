# INSTRUCTIONS

A NameSpace (NS) approach is used: each namespace is representing a web-client. In the host VM/server prepare a virtual bridge including the interface towards the web-server. 

`sudo ip link add <bridge-name> type bridge`  
`sudo ip link set <bridge-name> up`  
`sudo ip link set <iface-to-server> master <bridge-name>`  

Every NS shall create a pair of _veth_: one will be attached to the bridge, in ordre to link at L2 the set of web-clients and the web-server. 
