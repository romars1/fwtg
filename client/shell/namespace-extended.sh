#!/bin/bash
# FWTG
# Copyright (C) 2022  RomARS
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# 
# The code was developed by Mattia Quadrini <quadrini@romars.tech>
#
#
# IMPORTANT: IT IS REQUIRED THE NIC IS ATTACHED TO A VIRTUAL BRIDGE. 
# IT IS REQUIRED A DHCP SERVER IN
# LAN: 10.1.0.0/22 WITH Gateway 10.1.0.1/22

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit 0
fi

if [[ "$#" -eq 0 || "$#" -lt 15 ]]
then
        echo "==========================="
        echo "|      FWTG-Namespace       "
        echo "==========================="
        echo "| Inputs: "
        echo "| 1. N. Social"
        echo "| 2. N. Streaming"
        echo "| 3. N. Business"
        echo "| 4. N. Medical Data"
        echo "| 5. N. Medical Voice (y/n)"
        echo "| 6. N. Medical Video (y/n)"
        echo "| 7. N. IoT "
        echo "| 8. N. Green Services "
        echo "| 9. N. Staff"
        echo "| 10. Virtual-Bridge name"
        echo "| 11. Simulation Time"
        echo "| 12. Server IPv4"
        echo "| 13. Server Port"
        echo "| 14. Options: verbose/quiet"
        echo "| 15. Test name"
        echo "==========================="
        exit 0
fi

if [ "$#" -eq 1 ]
then 
	if [ "$1"=="clean" ] | [ "$1"=="clear" ]
	then
        	echo "Cleaning..."
		# Cleaning namespaces
		for i in $(ip netns list | awk '{print $1}')
		do
			ip netns del $i
		done
		# Cleaning weth (wise-eth)
		for v in $(ip l | grep weth | awk '{print $2}' | awk -F '@' '{print $1}')
		do
	        	ip link del $v
		done
		iptables -t nat -F POSTROUTING
		exit 0
	fi
fi

social=${1}
streaming=${2}
business=${3}
medical_data=${4}
medical_voice=${5}
medical_video=${6}
iot=${7}
green=${8}
staff=${9}
br=${10}
time=${11}
ip=${12}
port=${13}
verbose=${14} 
testname=${15}


total=`expr ${social} + ${streaming} + ${business} + ${medical_data} + ${medical_voice} + ${medical_video} + ${iot} + ${green}`

echo ">_Setup Environment"
# Actions for Social users
for client in `seq 1 ${social}`
do
	# Store names
	ns=social${client}
	vethin=weth-s${client}A # A is inside namespace
	vethout=weth-s${client}B # B is outside namespace
	if [[ "`ip l | grep $vethout`" ]]
        then
		if [[ `sudo ip netns exec ${ns} ping 10.1.0.1 -c 1` ]]
		then
			echo "Client-Social-${client} reused"
			continue
		else
                	#Assign address to veth-in (DHCP on VM)
			while ! [[ `sudo ip netns exec ${ns} ping 10.1.0.1 -c 1` ]]
                        do
                                ip netns exec ${ns} dhclient $vethin
                        done
			echo "Client-Social-${client} setup"
                	continue
		fi
        else
		# Create veth
		ip link add $vethout type veth peer name $vethin
		# Create namespace
		ip netns add ${ns}
		# Add veth-out to $br virtual bridge
		brctl addif $(echo $br) $vethout
		# Add veth-in in the namespace
		ip link set $vethin netns ${ns}
		# Swith on veths
		ip link set $vethout up
		ip netns exec ${ns} ip link set $vethin up
		# Assign address to veth-in (DHCP on VM)
		while ! [[ `sudo ip netns exec ${ns} ping 10.1.0.1 -c 1` ]]
			do
				ip netns exec ${ns} dhclient $vethin
			done
		echo "Client-Social-${client} setup"
	fi
done

# Actions for Streaming users
for client in `seq 1 ${streaming}`
do
        # Store names
        ns=streaming${client}
        vethin=weth-st${client}A # A is inside namespace
        vethout=weth-st${client}B # B is outside namespace
        if [[ "`ip l | grep $vethout`" ]]
	then
		if [[ `sudo ip netns exec ${ns} ping 10.1.0.1 -c 1` ]]
                then
                        echo "Client-Streaming-${client} reused"
                        continue
                else
                        #Assign address to veth-in (DHCP on VM)
                        while ! [[ `sudo ip netns exec ${ns} ping 10.1.0.1 -c 1` ]]
                        do
                                ip netns exec ${ns} dhclient $vethin
                        done
                        echo "Client-Streaming-${client} setup"
                        continue
                fi
	else
		# Create veth
        	ip link add $vethout type veth peer name $vethin
        	# Create namespace
        	ip netns add ${ns}
        	# Add veth-out to $br virtual bridge
        	brctl addif $(echo $br) $vethout
        	# Add veth-in in the namespace
        	ip link set $vethin netns ${ns}
        	# Swith on veths
        	ip link set $vethout up
        	ip netns exec ${ns} ip link set $vethin up
        	# Assign address to veth-in (DHCP on VM)
        	while ! [[ `sudo ip netns exec ${ns} ping 10.1.0.1 -c 1` ]]
                        do
                                ip netns exec ${ns} dhclient $vethin
                        done
		echo "Client-Streaming-${client} setup"
	fi
done

# Actions for Business users
for client in `seq 1 ${business}`
do
        # Store names
        ns=business${client}
        vethin=weth-b${client}A # A is inside namespace
        vethout=weth-b${client}B # B is outside namespace
        if [[ "`ip l | grep $vethout`" ]]
        then
                if [[ `sudo ip netns exec ${ns} ping 10.1.0.1 -c 1` ]]
                then
                        echo "Client-Business-${client} reused"
                        continue
                else
                        #Assign address to veth-in (DHCP on VM)
                        while ! [[ `sudo ip netns exec ${ns} ping 10.1.0.1 -c 1` ]]
                        do
                                ip netns exec ${ns} dhclient $vethin
                        done
                        echo "Client-Business-${client} setup"
                        continue
                fi
        else
		# Create veth
        	ip link add $vethout type veth peer name $vethin
       		# Create namespace
        	ip netns add ${ns}
        	# Add veth-out to $br virtual bridge
        	brctl addif $(echo $br) $vethout
        	# Add veth-in in the namespace
        	ip link set $vethin netns ${ns}
        	# Swith on veths
        	ip link set $vethout up
        	ip netns exec ${ns} ip link set $vethin up
        	# Assign address to veth-in (DHCP on VM)
       		while ! [[ `sudo ip netns exec ${ns} ping 10.1.0.1 -c 1` ]]
                        do
                                ip netns exec ${ns} dhclient $vethin
                        done
		echo "Client-Business-${client} setup"
	fi
done

# Actions for Staff users
for client in `seq 1 $staff`
do
        # Store names
        ns=staff${client}
        vethin=weth-sf${client}A # A is inside namespace
        vethout=weth-sf${client}B # B is outside namespace
        if [[ "`ip l | grep $vethout`" ]]
        then
                if [[ `sudo ip netns exec ${ns} ping 10.1.0.1 -c 1` ]]
                then
                        echo "Client-Staff-${client} reused"
                        continue
                else
                        #Assign address to veth-in (DHCP on VM)
                        while ! [[ `sudo ip netns exec ${ns} ping 10.1.0.1 -c 1` ]]
                        do
                                ip netns exec ${ns} dhclient $vethin
                        done
                        echo "Client-Staff-${client} setup"
                        continue
                fi
        else
		# Create veth
        	ip link add $vethout type veth peer name $vethin
       		# Create namespace
        	ip netns add ${ns}
        	# Add veth-out to $br virtual bridge
        	brctl addif $(echo $br) $vethout
        	# Add veth-in in the namespace
        	ip link set $vethin netns ${ns}
        	# Swith on veths
        	ip link set $vethout up
        	ip netns exec ${ns} ip link set $vethin up
        	# Assign address to veth-in (DHCP on VM)
       		while ! [[ `sudo ip netns exec ${ns} ping 10.1.0.1 -c 1` ]]
                        do
                                ip netns exec ${ns} dhclient $vethin
                        done
		echo "Client-Staff-${client} setup"
	fi
done

# Actions for Medical-Data users
for client in `seq 1 ${medical_data}`
do
        # Store names
        ns=medical_data${client}
        vethin=weth-md${client}A # A is inside namespace
        vethout=weth-md${client}B # B is outside namespace
        if [[ "`ip l | grep $vethout`" ]]
        then
                if [[ `sudo ip netns exec ${ns} ip addr show | grep 10.1.0.10/22` ]]
                then
                        echo "Client-Medical-Data-${client} reused"
                        continue
                else
                        #Assign address to veth-in (FIXED)
                        ip netns exec ${ns} ip addr add 10.1.0.10/22 dev $vethin
                fi
        else
		# Create veth
        	ip link add $vethout type veth peer name $vethin
       		# Create namespace
        	ip netns add ${ns}
        	# Add veth-out to $br virtual bridge
        	brctl addif $(echo $br) $vethout
        	# Add veth-in in the namespace
        	ip link set $vethin netns ${ns}
        	# Swith on veths
        	ip link set $vethout up
        	ip netns exec ${ns} ip link set $vethin up
        	# Assign address to veth-in (FIXED)
		ip netns exec ${ns} ip addr add 10.1.0.10/22 dev $vethin
		ip netns exec ${ns} ip route add default via 10.1.0.1 dev $vethin
		echo "Client-Medical-Data-${client} setup"
	fi
done

# Actions for Medical-Voice users
for client in `seq 1 ${medical_voice}`
do
        # Store names
        ns=medical_voice${client}
        vethin=weth-mv${client}A # A is inside namespace
        vethout=weth-mv${client}B # B is outside namespace
        if [[ "`ip l | grep $vethout`" ]]
        then
                if [[ `sudo ip netns exec ${ns} ip addr show | grep 10.1.0.11/22` ]]
                then
                        echo "Client-Medical-Voice-${client} reused"
                        continue
                else
                        #Assign address to veth-in (FIXED)
                        ip netns exec ${ns} ip addr add 10.1.0.11/22 dev $vethin
                fi
        else
		# Create veth
        	ip link add $vethout type veth peer name $vethin
       		# Create namespace
        	ip netns add ${ns}
        	# Add veth-out to $br virtual bridge
        	brctl addif $(echo $br) $vethout
        	# Add veth-in in the namespace
        	ip link set $vethin netns ${ns}
        	# Swith on veths
        	ip link set $vethout up
        	ip netns exec ${ns} ip link set $vethin up
        	# Assign address to veth-in (FIXED)
		ip netns exec ${ns} ip addr add 10.1.0.11/22 dev $vethin
		ip netns exec ${ns} ip route add default via 10.1.0.1 dev $vethin
		echo "Client-Medical-Data-${client} setup"
	fi
done

# Actions for Medical-Video users
for client in `seq 1 ${medical_video}`
do
        # Store names
        ns=medical_video${client}
        vethin=weth-mvd${client}A # A is inside namespace
        vethout=weth-mvd${client}B # B is outside namespace
        if [[ "`ip l | grep $vethout`" ]]
        then
                if [[ `sudo ip netns exec ${ns} ip addr show | grep 10.1.0.12/22` ]]
                then
                        echo "Client-Medical-Video-${client} reused"
                        continue
                else
                        #Assign address to veth-in (FIXED)
                        ip netns exec ${ns} ip addr add 10.1.0.12/22 dev $vethin
                fi
        else
		# Create veth
        	ip link add $vethout type veth peer name $vethin
       		# Create namespace
        	ip netns add ${ns}
        	# Add veth-out to $br virtual bridge
        	brctl addif $(echo $br) $vethout
        	# Add veth-in in the namespace
        	ip link set $vethin netns ${ns}
        	# Swith on veths
        	ip link set $vethout up
        	ip netns exec ${ns} ip link set $vethin up
        	# Assign address to veth-in (FIXED)
		ip netns exec ${ns} ip addr add 10.1.0.11/22 dev $vethin
		ip netns exec ${ns} ip route add default via 10.1.0.1 dev $vethin
		echo "Client-Medical-Video-${client} setup"
	fi
done

# Actions for IoT users
for client in `seq 1 ${iot}`
do
        # Store names
        ns=iot${client}
        vethin=weth-i${client}A # A is inside namespace
        vethout=weth-i${client}B # B is outside namespace
        if [[ "`ip l | grep $vethout`" ]]
        then
                if [[ `sudo ip netns exec ${ns} ip addr show | grep 10.1.0.13/22` ]]
                then
                        echo "Client-IoT-${client} reused"
                        continue
                else
                        #Assign address to veth-in (FIXED)
                        ip netns exec ${ns} ip addr add 10.1.0.13/22 dev $vethin
                fi
        else
		# Create veth
        	ip link add $vethout type veth peer name $vethin
       		# Create namespace
        	ip netns add ${ns}
        	# Add veth-out to $br virtual bridge
        	brctl addif $(echo $br) $vethout
        	# Add veth-in in the namespace
        	ip link set $vethin netns ${ns}
        	# Swith on veths
        	ip link set $vethout up
        	ip netns exec ${ns} ip link set $vethin up
        	# Assign address to veth-in (FIXED)
		ip netns exec ${ns} ip addr add 10.1.0.13/22 dev $vethin
		ip netns exec ${ns} ip route add default via 10.1.0.1 dev $vethin
		echo "Client-IoT-${client} setup"
	fi
done

# Actions for Green-Services users
for client in `seq 1 ${green}`
do
        # Store names
        ns=green${client}
        vethin=weth-g${client}A # A is inside namespace
        vethout=weth-g${client}B # B is outside namespace
        if [[ "`ip l | grep $vethout`" ]]
        then
                if [[ `sudo ip netns exec ${ns} ip addr show | grep 10.1.0.14/22` ]]
                then
                        echo "Client-Green-${client} reused"
                        continue
                else
                        #Assign address to veth-in (FIXED)
                        ip netns exec ${ns} ip addr add 10.1.0.14/22 dev $vethin
                fi
        else
		# Create veth
        	ip link add $vethout type veth peer name $vethin
       		# Create namespace
        	ip netns add ${ns}
        	# Add veth-out to $br virtual bridge
        	brctl addif $(echo $br) $vethout
        	# Add veth-in in the namespace
        	ip link set $vethin netns ${ns}
        	# Swith on veths
        	ip link set $vethout up
        	ip netns exec ${ns} ip link set $vethin up
        	# Assign address to veth-in (FIXED)
		ip netns exec ${ns} ip addr add 10.1.0.14/22 dev $vethin
		ip netns exec ${ns} ip route add default via 10.1.0.1 dev $vethin
		echo "Client-Green-${client} setup"
	fi
done

echo ">_NET-NS list"
ip netns list
echo ">_Start clients"

sleep=`expr 3600 + $total / 2`

# Get name of all weths
#i=1; for v in $(ip l | grep weth | awk '{print $2}' | awk -F '@' '{print $1}'); do weths[$i]="$v,";i=$i+1; done
# Monitoring using bmon on all interfaces (warning, big txt file)
$(sleep $sleep && killall bmon) & $(bmon -r 1 -o format:fmt='$(element:name):\t TX $(attr:txrate:bytes) B\t RX $(attr:rxrate:bytes) B\n' > $(echo $testname).rate) &

# MEDIUM
for client in `seq 1 ${social}`
do
	ns=social${client}
        vethin=weth-s${client}A # A is inside namespace
        vethout=weth-s${client}B # B is outside namespace
	# Start single client
        ip netns exec ${ns} ip r del default
        ip netns exec ${ns} ip r add default via 10.1.0.22	
	ip netns exec ${ns} python3.9 ./main.py social $time $ip $port $verbose &
	sleep 0.5
done

# PREMIUM
for client in `seq 1 ${streaming}`
do
        ns=streaming${client}
        vethin=weth-s${client}A # A is inside namespace
        vethout=weth-s${client}B # B is outside namespace
	# Start single client
	ip netns exec ${ns} ip r del default 
	ip netns exec ${ns} ip r add default via 10.1.0.20
        ip netns exec ${ns} python3.9 ./main.py streaming $time $ip $port $verbose &
        sleep 0.5
done

# BASIC
for client in `seq 1 ${business}`
do
        ns=business${client}
        vethin=weth-s${client}A # A is inside namespace
        vethout=weth-s${client}B # B is outside namespace
	# Start single client
        ip netns exec ${ns} ip r del default
        ip netns exec ${ns} ip r add default via 10.1.0.22	
	ip netns exec ${ns} python3.9 ./main.py business $time $ip $port $verbose &
        sleep 0.5
done

# STAFF
for client in `seq 1 $staff`
do
	ns=staff${client}
        vethin=weth-sf${client}A # A is inside namespace
        vethout=weth-sf${client}B # B is outside namespace
	# Start single client
        ip netns exec ${ns} ip r del default
        ip netns exec ${ns} ip r add default via 10.1.0.21	
	ip netns exec ${ns} python3.9 ./main.py streaming $time $ip $port $verbose &
	sleep 0.5
done



for client in `seq 1 ${medical_data}`
do
        ns=medical_data${client}
        vethin=weth-md${client}A # A is inside namespace
        vethout=weth-md${client}B # B is outside namespace
        # Start single client
        ip netns exec ${ns} python3.9 ./main.py medical_data $time $ip $port $verbose &
        sleep 0.5
done

for client in `seq 1 ${medical_voice}`
do
        ns=medical_voice${client}
        vethin=weth-mv${client}A # A is inside namespace
        vethout=weth-mv${client}B # B is outside namespace
        # Start single client
        ip netns exec ${ns} python3.9 ./main.py medical_voice $time $ip $port $verbose &
        sleep 0.5
done

for client in `seq 1 ${medical_video}`
do
        ns=medical_video${client}
        vethin=weth-mvd${client}A # A is inside namespace
        vethout=weth-mvd${client}B # B is outside namespace
        # Start single client
        ip netns exec ${ns} python3.9 ./main.py medical_video $time $ip $port $verbose &
        sleep 0.5
done

for client in `seq 1 ${iot}`
do
        ns=iot${client}
        vethin=weth-i${client}A # A is inside namespace
        vethout=weth-i${client}B # B is outside namespace
        # Start single client
        ip netns exec ${ns} python3.9 ./main.py iot $time $ip $port $verbose &
        sleep 0.5
done

for client in `seq 1 ${green}`
do
        ns=green${client}
        vethin=weth-g${client}A # A is inside namespace
        vethout=weth-g${client}B # B is outside namespace
        # Start single client
        ip netns exec ${ns} python3.9 ./main.py green $time $ip $port $verbose &
        sleep 0.5
done
