#!/bin/bash
# FWTG
# Copyright (C) 2022  RomARS
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# 
# The code was developed by Mattia Quadrini <quadrini@romars.tech>
#
#
# Bash script to start single python script emulating multiple users. 
# Indicate the number of each user in the input,
# the script will start a single python for each user. Maximum recommended 
# number of users is 50÷100.

if [[ $# -eq 1 ]] 
then
        if [[ "$1" == "help" || "$1" == "-h" ]] 
        then
                clear
                echo "--------------------------------------------------------------------------------"
                echo "] Web-Traffic-Generator for Multiple User"
                echo "]"
                echo "] Mandatory inputs are:"
                echo "]  - Social Users [integer]"
                echo "]  - Streaming Users [integer]"
                echo "]  - Business Users [integer]"
                echo "]  - Simulation Time [s]"
                echo "] Use 'verbose' to enable terminal output"
                echo "]"
                echo "--------------------------------------------------------------------------------"
                exit 1
        fi
elif [[ $# -eq 0 ]]
then
        clear
        echo "------------------------------------------"
        echo "] Web-Traffic-Generator for Multiple User "
        echo "]"
        echo "] Try with 'help' or '-h'"
        echo "------------------------------------------"
        exit 1
fi

#social=$(jq '(.social.distribution)' ./json/user_distribution.json)
#streaming=$(jq '(.streaming.distribution)' ./json/user_distribution.json)
#business=$(jq '(.business.distribution)' ./json/user_distribution.json) 
social=$1;
streaming=$2;
business=$3;
time=$4;
verbose=$5;

for i in `seq 1 "$social"`
do
	python3.9 ./main.py social $(echo $time) $SERVER_IP $SERVER_PORT $(echo $verbose) &
done 

for i in `seq 1 "$streaming"`
do
	python3.9 ./main.py streaming $(echo $time) $SERVER_IP $SERVER_PORT $(echo $verbose) &
done

for i in `seq 1 "$business"`
do
	python3.9 ./main.py business $(echo $time) $SERVER_IP $SERVER_PORT $(echo $verbose) &
done

echo "Started at $(date +%H:%M:%S)"
while [[ $(ps aux | grep 'main.py' | cat -n | awk '{print $1}' | tail -n 1) -gt 1 ]]
do
        sleep 1;
done

# Prepare TAR for export
name="client-$(hostname)"
mkdir $name && \
mv log/*.txt $name/
tar -a -cf $name.tar.gz $name/
mv $name.tar.gz delivery/$name.tar.gz

echo "] Emulation finished. Please check your og files."
exit 0
