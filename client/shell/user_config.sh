#!/bin/bash

# FWTG
# Copyright (C) 2022  RomARS
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# 
# The code was developed by Mattia Quadrini <quadrini@romars.tech>
#

##################################
#
# Modify values in the VARIABLES
# section. These shal be updated 
# in the new Docker Containers
#
##################################

debug=false

## VARIABLES

# user_distribution.json 
social_distribution=10
streaming_distribution=10
business_distribution=10

social_activity=60
streaming_activity=35
business_activity=40

# user_profile.json 
# (info) Bash array referred to (newspaper social_network media_streaming collaborative)
social=(0 70 30 0)
streaming=(0 50 50 0)
business=(40 10 10 40)

## RUN 
root=$(pwd)
cd ./client/json
mv user_distribution.json user_distribution.json.old 
mv user_profile.json user_profile.json.old #


# Distribution
jq "(.social.distribution=$social_distribution | .streaming.distribution=$streaming_distribution | .business.distribution=$business_distribution | .social.user_activity=$social_activity | .streaming.user_activity=$streaming_activity | .business.user_activity=$business_activity)" user_distribution.json.old > user_distribution.json 
# Profile
jq "(.social.newspaper=${social[0]} | .social_network=${social[1]} | .social.media_streaming=${social[2]} | .social.collaborative=${social[3]} | .streaming.newspaper=${streaming[0]} | .streaming.social_network=${streaming[1]} | .streaming.media_streaming=${streaming[2]} | .streaming.collaborative=${streaming[3]} | .business.newspaper=${business[0]} | .business.social_network=${business[1]} | .business.media_streaming=${business[2]} | .business.collaborative=${business[3]})" user_profile.json.old > user_profile.json 
# Clean old files
rm user_distribution.json.old 
rm user_profile.json.old 


answer=""
while [ "$answer" == "" ]
do
	echo "Print json files? (y/n)"
	read answer
done

if [ "$answer" == "y" ]
then 
	echo ""
	echo "** USER_DISTRIBUTION.json **"
	cat user_distribution.json
	echo ""
	echo "** USER_PROFILE.json **"
	cat user_profile.json
	echo ""
fi

answer=""
while [ "$answer" == "" ]
do
	echo "Build FWTG-Client? (y/n)"
	read answer
done

if [ "$answer" == "y" ]
then 
	cd $root
	echo "Requires sudo."
	sudo bash build.sh client
	exit 0
fi

# END OF FILE #
