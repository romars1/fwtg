#!/bin/bash
# FWTG
# Copyright (C) 2022  RomARS
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# 
# The code was developed by Mattia Quadrini <quadrini@romars.tech>
#
#
# THIS SCRIPT SHALL BE EXECUTED IN LOCAL.
# NOTE
# A virtual-bridge is required. Both physical and virtual interfaces shall be added.
 

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit 0
fi

if [[ "$#" -eq 0  || "$#" -lt 9 ]]
then
        echo "==========================="
        echo "|      FWTG-Namespace        "
        echo "==========================="
        echo "| Inputs: "
        echo "| 1. N. Social"
        echo "| 2. N. Streaming"
        echo "| 3. N. Business"
        echo "| 4. Virtual-Bridge name"
        echo "| 5. Simulation Time"
        echo "| 6. Server IPv4"
        echo "| 7. Server Port"
        echo "| 8. Options: verbose/quiet"
        echo "| 9. Test name"
        echo "==========================="
	exit 0
fi

if [ "$#" -eq 1 ]
then 
	if [ "$1"=="clean" ] | [ "$1"=="clear" ]
	then
        	echo "Cleaning..."
		# Cleaning namespaces
		for i in $(ip netns list | awk '{print $1}')
		do
			ip netns del $i
		done
		# Cleaning weth (wise-eth)
		for v in $(ip l | grep weth | awk '{print $2}' | awk -F '@' '{print $1}')
		do
	        	ip link del $v
		done
		exit 0
	fi
fi

# VARIABLES DEFINITION

social=$1
streaming=$2
business=$3
br=$4
time=$5
ip=$6
port=$7
verbose=$8 
testname=$9

total=`expr ${social} + ${streaming} + ${business}`

echo ">_Setup Environment"
# Actions for Social users
for client in `seq 1 ${social}`
do
	# Store names
	ns=social$(echo ${client})
	vethin=weth-s$(echo ${client})A # A is inside namespace
	vethout=weth-s$(echo ${client})B # B is outside namespace
	if [[ "`ip l | grep $vethout`" ]]
        then
		if [[ `sudo ip netns exec ${ns} ping 10.1.0.1 -c 1` ]]
		then
			echo "Client-Social-$(echo ${client}) reused"
			continue
		else
                	#Assign address to veth-in (DHCP on Router-LAN VM in XEON3)
			while ! [[ `sudo ip netns exec ${ns} ping 10.1.0.1 -c 1` ]]
                        do
                                ip netns exec ${ns} dhclient $vethin
                        done
			echo "Client-Social-$(echo ${client}) setup"
                	continue
		fi
        else
		# Create veth
		ip link add $vethout type veth peer name $vethin
		# Create namespace
		ip netns add ${ns}
		# Add veth-out to ${br} virtual bridge
		brctl addif $(echo ${br}) $vethout
		# Add veth-in in the namespace
		ip link set $vethin netns ${ns}
		# Swith on veths
		ip link set $vethout up
		ip netns exec ${ns} ip link set $vethin up
		# Assign address to veth-in (DHCP on Router-LAN VM in XEON3)
		while ! [[ `sudo ip netns exec ${ns} ping 10.1.0.1 -c 1` ]]
			do
				ip netns exec ${ns} dhclient $vethin
			done
		echo "Client-Social-$(echo ${client}) setup"
	fi
done

# Actions for Streaming users
for client in `seq 1 ${streaming}`
do
        # Store names
        ns=streaming$(echo ${client})
        vethin=weth-st$(echo ${client})A # A is inside namespace
        vethout=weth-st$(echo ${client})B # B is outside namespace
        if [[ "`ip l | grep $vethout`" ]]
	then
		if [[ `sudo ip netns exec ${ns} ping 10.1.0.1 -c 1` ]]
                then
                        echo "Client-Streaming-$(echo ${client}) reused"
                        continue
                else
                        #Assign address to veth-in (DHCP on Router-LAN VM in XEON3)
                        while ! [[ `sudo ip netns exec ${ns} ping 10.1.0.1 -c 1` ]]
                        do
                                ip netns exec ${ns} dhclient $vethin
                        done
                        echo "Client-Streaming-$(echo ${client}) setup"
                        continue
                fi
	else
		# Create veth
        	ip link add $vethout type veth peer name $vethin
        	# Create namespace
        	ip netns add ${ns}
        	# Add veth-out to ${br} virtual bridge
        	brctl addif $(echo ${br}) $vethout
        	# Add veth-in in the namespace
        	ip link set $vethin netns ${ns}
        	# Swith on veths
        	ip link set $vethout up
        	ip netns exec ${ns} ip link set $vethin up
        	# Assign address to veth-in (DHCP on Router-LAN VM in XEON3)
        	while ! [[ `sudo ip netns exec ${ns} ping 10.1.0.1 -c 1` ]]
                        do
                                ip netns exec ${ns} dhclient $vethin
                        done
		echo "Client-Streaming-$(echo ${client}) setup"
	fi
done

# Actions for Business users
for client in `seq 1 ${business}`
do
        # Store names
        ns=business$(echo ${client})
        vethin=weth-b$(echo ${client})A # A is inside namespace
        vethout=weth-b$(echo ${client})B # B is outside namespace
        if [[ "`ip l | grep $vethout`" ]]
        then
                if [[ `sudo ip netns exec ${ns} ping 10.1.0.1 -c 1` ]]
                then
                        echo "Client-Business-$(echo ${client}) reused"
                        continue
                else
                        #Assign address to veth-in (DHCP on Router-LAN VM in XEON3)
                        while ! [[ `sudo ip netns exec ${ns} ping 10.1.0.1 -c 1` ]]
                        do
                                ip netns exec ${ns} dhclient $vethin
                        done
                        echo "Client-Business-$(echo ${client}) setup"
                        continue
                fi
        else
		# Create veth
        	ip link add $vethout type veth peer name $vethin
       		# Create namespace
        	ip netns add ${ns}
        	# Add veth-out to ${br} virtual bridge
        	brctl addif $(echo ${br}) $vethout
        	# Add veth-in in the namespace
        	ip link set $vethin netns ${ns}
        	# Swith on veths
        	ip link set $vethout up
        	ip netns exec ${ns} ip link set $vethin up
        	# Assign address to veth-in (DHCP on Router-LAN VM in XEON3)
       		while ! [[ `sudo ip netns exec ${ns} ping 10.1.0.1 -c 1` ]]
                        do
                                ip netns exec ${ns} dhclient $vethin
                        done
		echo "Client-Business-$(echo ${client}) setup"
	fi
done

echo ">_NET-NS list"
ip netns list
echo ">_Start clients"

sleep=`expr 3600 + $total / 2`

# Get name of all weths
#i=1; for v in $(ip l | grep weth | awk '{print $2}' | awk -F '@' '{print $1}'); do weths[$i]="$v,";i=$i+1; done
# Monitoring using bmon on all interfaces (warning, big txt file)
$(sleep $sleep && killall bmon) & $(bmon -p "${br}" -r 1 -o format:fmt='$(element:name):\t TX $(attr:txrate:bytes) B\t RX $(attr:rxrate:bytes) B\n' > $(echo ${testname}).rate) &

for client in `seq 1 ${social}`
do
	ns=social$(echo ${client})
        vethin=weth-s${client}A # A is inside namespace
        vethout=weth-s${client}B # B is outside namespace
        # Start single client
        ip netns exec ${ns} python3.9 ./main.py social $time ${ip} ${port} ${verbose} &
	sleep 0.5
done

for client in `seq 1 ${streaming}`
do
        ns=streaming${client}
        vethin=weth-s${client}A # A is inside namespace
        vethout=weth-s${client}B # B is outside namespace
        # Start single client
        ip netns exec ${ns} python3.9 ./main.py streaming $time ${ip} ${port} ${verbose} &
        sleep 0.5
done

for client in `seq 1 ${business}`
do
        ns=business${client}
        vethin=weth-s${client}A # A is inside namespace
        vethout=weth-s${client}B # B is outside namespace
        # Start single client
        ip netns exec ${ns} python3.9 ./main.py business $time ${ip} ${port} ${verbose} &
        sleep 0.5
done

#for client in `seq 1 ${social}`
#do
#        ns=social$(echo ${client})
#        vethin=weth-s$(echo ${client})A # A is inside namespace
#        vethout=weth-s$(echo ${client})B # B is outside namespace
#        # Start single client
#        ip netns exec ${ns} python3.9 ./main.py social $time ${ip} ${port} ${verbose} &
#        sleep 0.5
#done
#
#for client in `seq 1 ${streaming}`
#do
#        ns=streaming$(echo ${client})
#        vethin=weth-s$(echo ${client})A # A is inside namespace
#        vethout=weth-s$(echo ${client})B # B is outside namespace
#        # Start single client
#        ip netns exec ${ns} python3.9 ./main.py streaming $time ${ip} ${port} ${verbose} &
#        sleep 0.5
#done
#
#for client in `seq 1 ${business}`
#do
#        ns=business$(echo ${client})
#        vethin=weth-s$(echo ${client})A # A is inside namespace
#        vethout=weth-s$(echo ${client})B # B is outside namespace
#        # Start single client
#        ip netns exec ${ns} python3.9 ./main.py business $time ${ip} ${port} ${verbose} &
#        sleep 0.5
#done