#!/bin/bash
# FWTG
# Copyright (C) 2022  RomARS
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# 
# The code was developed by Mattia Quadrini <quadrini@romars.tech>
#
# 
# The code was developed by Mattia Quadrini <quadrini@romars.tech>

if [[ "$1" == "help" || "$1" == "-h" || $# -eq 0 ]]
then
        clear
        echo "--------------------------------------------------------------------------------"
        echo "] Web-Traffic-Generator for Multiple User"
        echo "]"
        echo "] Mandatory inputs are:"
        echo "]  1 Social Users [integer]"
        echo "]  2 Streaming Users [integer]"
        echo "]  3 Business Users [integer]"
        echo "]  4 Simulation Time [s]"
        echo "]  5 Server IP"
        echo "]  6 Server port"
        echo "]  7 Use 'verbose' to enable terminal output"
        echo "]"
        echo "--------------------------------------------------------------------------------"
        exit 1
fi

social=$1;
streaming=$2;
business=$3;
time=$4;
ip=$5;
port=$6;
verbose=$7;

PID_LIST=()

for i in `seq 1 ${social}`
do
        python3.9 ./main.py social ${time} ${ip} ${port} ${verbose} &
        PID_LIST+=($!)
        sleep 0.5
done

for i in `seq 1 ${streaming}`
do
        python3.9 ./main.py streaming ${time} ${ip} ${port} ${verbose} &
        PID_LIST+=($!)
        sleep 0.5
done

for i in `seq 1 ${business}`
do
        python3.9 ./main.py business ${time} ${ip} ${port} ${verbose} &
        PID_LIST+=($!)
        sleep 0.5
done

function terminate()
{
        sudo kill -SIGTERM ${PID_LIST[${#PID_LIST[@]}-2]} ${PID_LIST[${#PID_LIST[@]}-1]}
        sleep 2
}

trap terminate SIGINT
wait ${PID_LIST}
