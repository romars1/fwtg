#!/bin/bash
# FWTG
# Copyright (C) 2022  RomARS
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# 
# The code was developed by Mattia Quadrini <quadrini@romars.tech>
#

# THIS SCRIPT SHALL BE USED FOR DOCKER DEPLOYMENT. 
# SERVER_IP AND SERVER_PORT ARE ENVIRONMENT VARIABLE

if [[ $# -eq 1 ]] 
then
        if [[ "$1" == "help" || "$1" == "-h" || "$1" == "--help" || $# -eq 0 ]] 
        then
                clear
                echo "--------------------------------------------------------------------------------"
                echo "] Web-Traffic-Generator"
                echo "]"
                echo "] Mandatory inputs are:"
                echo "]  1. Simulation Time [s]"
		echo "]  2. User Profile [social - business - streaming]"
                echo "]  3. Optionally tou can use 'verbose' to enable terminal output"
                echo "]"
                echo "]  Use ./json/user_distribution.json to indicate the number of each user profile"
                echo "--------------------------------------------------------------------------------"
                exit 1
        fi

python3.9 ./main.py $1 $2 $SERVER_IP $SERVER_PORT $3 

name="client-$(hostname)"
mkdir $name && \
mv log/*.txt $name/
tar -a -cf $name.tar.gz $name/
mv $name.tar.gz delivery/$name.tar.gz

echo "] Emulation finished. Please check your og files."
exit 0