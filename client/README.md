# FWTG-Client

The client implements most of the logic of the software. In fact, it requests or send objects at a given rate and with a given size according to some configuration and distribution provided as paramters or external files after an analysis approach. 

## Requirements
For the correct installation of the fwtg-client the following system requirements must be satisfied:
 - Ubuntu 20.04.1 (tested)
 - jq v1.6
 - iperf3 (recommened)
 - python 3.9
 - python3-pip
 - "requests >= 2.22.0" module (`python3.9 -m pip install requests`)
 - "numpy >= 1.21.1" module (`python3.9 -m pip install numpy`)
 - "iperf3 >=0.1.11" module (`python3.9 -m pip install iperf3`)
 - "docker-compose > v1.25.0 (v2 tested)" (`git clone https://github.com/docker/compose.git --branch v2 --single-branch`)

## Client structure 

`./client/`  
 ⎿ `../csv/`
 ⎿ `../extension/`
 ⎿ `../json/`
 ⎿ `../log/` 
 ⎿ `../module/`
 ⎿ `../shell/`


### Software usage
Launch the `main.py` providing input parameters. Type `main.py help` to see the list of possible inputs. 

For a better user experience you can use the bash scripts in the `./client/` or `./client/shell/` folders to start multiple clients, random profiles, custom profiles. 

### Software customization
The `csv` folder contains the `.csv` files representing the distribution of request-rate and object-size. These are used to drive the request/send of objects by the client. Files are divided per Service Category and Interaction Modes.  

In the `extension` folder additional scripts are provided to support extra functions.  

The `json` folder contains the _user_profile.json_ that must be configured to have different profiles.  

In `module` folder you can modify the logic of the software:  
 - `loader.py` – it is the core of the software. All main functions are implemented here, with some hard-coded parameters you can change.  
 - `config.py` – it contains few general-purpose functions used in the `loader.py`.  
 - `client.py` – it implements the HTTP-client using the _request_ library.  

The `shell` folder collects miscellaneous bash scripts to start the client using namespaces, for instance. 

This scripts are used mainly in Docker deployments. Please configure the `docker-compose.yaml` file and read the instructions contained in the `docker` folder. 

### Logs and output
Logs of the emualtion are contained in the `log` folder. To improve performance please do not enable output at the client/server side, instead redirect the output to a external file

```
python3.9 ./main.py custom $EMULATION_TIME $SERVER_IP $SERVER_PORT verbose &> ./log/log.user.txt
```


