#!/bin/bash
# FWTG
# Copyright (C) 2022  RomARS
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# 
# The code was developed by Mattia Quadrini <quadrini@romars.tech>
#

./autoconfig.sh

if ! [ -d "log" ]
then
    mkdir log
fi
userIndex=1
name="log-user-custom-"${userIndex}".txt"
while [ true ]
do
    if [ -e "./log/${name}" ]; 
    then 
        userIndex=`expr ${userIndex} + 1`
        name="log-user-custom-"${userIndex}".txt"
    else 
        name="log-user-custom-"${userIndex}".txt"
        break
    fi
done

python3.9 ./main.py custom ${EMUL_TIME} ${SERVER_IP} ${SERVER_PORT} verbose &> ./log/${name} &

sleep inf